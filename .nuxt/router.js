import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const _a9ae02ba = () => import('../resources/pages/manage.vue' /* webpackChunkName: "pages/manage" */).then(m => m.default || m)
const _43528234 = () => import('../resources/pages/manage/index.vue' /* webpackChunkName: "pages/manage/index" */).then(m => m.default || m)
const _73b8687e = () => import('../resources/pages/manage/account.vue' /* webpackChunkName: "pages/manage/account" */).then(m => m.default || m)
const _88254892 = () => import('../resources/pages/forgot-password.vue' /* webpackChunkName: "pages/forgot-password" */).then(m => m.default || m)
const _bb1dc0ca = () => import('../resources/pages/login.vue' /* webpackChunkName: "pages/login" */).then(m => m.default || m)
const _4e6d06f8 = () => import('../resources/pages/index.vue' /* webpackChunkName: "pages/index" */).then(m => m.default || m)



if (process.client) {
  window.history.scrollRestoration = 'manual'
}
const scrollBehavior = function (to, from, savedPosition) {
  // if the returned position is falsy or an empty object,
  // will retain current scroll position.
  let position = false

  // if no children detected
  if (to.matched.length < 2) {
    // scroll to the top of the page
    position = { x: 0, y: 0 }
  } else if (to.matched.some((r) => r.components.default.options.scrollToTop)) {
    // if one of the children has scrollToTop option set to true
    position = { x: 0, y: 0 }
  }

  // savedPosition is only available for popstate navigations (back button)
  if (savedPosition) {
    position = savedPosition
  }

  return new Promise(resolve => {
    // wait for the out transition to complete (if necessary)
    window.$nuxt.$once('triggerScroll', () => {
      // coords will be used if no selector is provided,
      // or if the selector didn't match any element.
      if (to.hash) {
        let hash = to.hash
        // CSS.escape() is not supported with IE and Edge.
        if (typeof window.CSS !== 'undefined' && typeof window.CSS.escape !== 'undefined') {
          hash = '#' + window.CSS.escape(hash.substr(1))
        }
        try {
          if (document.querySelector(hash)) {
            // scroll to anchor by returning the selector
            position = { selector: hash }
          }
        } catch (e) {
          console.warn('Failed to save scroll position. Please add CSS.escape() polyfill (https://github.com/mathiasbynens/CSS.escape).')
        }
      }
      resolve(position)
    })
  })
}


export function createRouter () {
  return new Router({
    mode: 'history',
    base: '/',
    linkActiveClass: 'is-active',
    linkExactActiveClass: 'is-active',
    scrollBehavior,
    routes: [
		{
			path: "/manage",
			component: _a9ae02ba,
			children: [
				{
					path: "",
					component: _43528234,
					name: "manage.index"
				},
				{
					path: "account",
					component: _73b8687e,
					name: "account.index"
				}
			]
		},
		{
			path: "/forgot-password",
			component: _88254892,
			name: "app.forgot-password"
		},
		{
			path: "/login",
			component: _bb1dc0ca,
			name: "app.login"
		},
		{
			path: "/",
			component: _4e6d06f8,
			name: "app.index"
		}
    ],
    
    
    fallback: false
  })
}
