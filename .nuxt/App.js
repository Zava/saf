import Vue from 'vue'
import NuxtLoading from './components/nuxt-loading.vue'

import '../resources/assets/scss/main.scss'


let layouts = {

  "_auth": () => import('../resources/layouts/auth.vue'  /* webpackChunkName: "layouts/auth" */).then(m => m.default || m),

  "_default": () => import('../resources/layouts/default.vue'  /* webpackChunkName: "layouts/default" */).then(m => m.default || m)

}

let resolvedLayouts = {}

export default {
  head: {"meta":[{"charset":"utf-8"},{"name":"viewport","content":"width=device-width,initial-scale=1"},{"http-equiv":"x-ua-compatible","content":"ie=edge,chrome=1"}],"link":[{"rel":"stylesheet","href":"https:\u002F\u002Ffonts.googleapis.com\u002Fcss?family=Montserrat:300,400,600"},{"rel":"stylesheet","href":"https:\u002F\u002Fcdn.materialdesignicons.com\u002F2.0.46\u002Fcss\u002Fmaterialdesignicons.min.css"},{"rel":"shortcut icon","type":"image\u002Fx-icon","href":"\u002Ffavicon.ico"},{"rel":"apple-touch-icon","size":"192x192","href":"\u002Fmobile-icon.png"},{"rel":"icon","type":"image\u002Fpng","size":"192x192","href":"\u002Fmobile-icon.png"},{"rel":"icon","type":"image\u002Fpng","size":"32x32","href":"\u002Ffavicon.png"}],"style":[],"script":[],"htmlAttrs":{"lang":"id"},"noscript":[{"innerHtml":"\u003Ch1\u003EJavascript Disabled\u003C\u002Fh1\u003E\u003Cp\u003EIt appears that you do not have Javascript enabled. This application relies on Javascript for most of our features.\u003Cp\u003E\u003Cp\u003EPlease enable Javascript and \u003Ca href=\".\"\u003Ereload\u003C\u002Fa\u003E in order to use this site.\u003C\u002Fp\u003E"}]},
  render(h, props) {
    const loadingEl = h('nuxt-loading', { ref: 'loading' })
    const layoutEl = h(this.layout || 'nuxt')
    const templateEl = h('div', {
      domProps: {
        id: '__layout'
      },
      key: this.layoutName
    }, [ layoutEl ])

    const transitionEl = h('transition', {
      props: {
        name: 'layout',
        mode: 'out-in'
      }
    }, [ templateEl ])

    return h('div',{
      domProps: {
        id: '__nuxt'
      }
    }, [
      loadingEl,
      transitionEl
    ])
  },
  data: () => ({
    layout: null,
    layoutName: ''
  }),
  beforeCreate () {
    Vue.util.defineReactive(this, 'nuxt', this.$options.nuxt)
  },
  created () {
    // Add this.$nuxt in child instances
    Vue.prototype.$nuxt = this
    // add to window so we can listen when ready
    if (typeof window !== 'undefined') {
      window.$nuxt = this
    }
    // Add $nuxt.error()
    this.error = this.nuxt.error
  },
  
  mounted () {
    this.$loading = this.$refs.loading
  },
  watch: {
    'nuxt.err': 'errorChanged'
  },
  
  methods: {
    
    errorChanged () {
      if (this.nuxt.err && this.$loading) {
        if (this.$loading.fail) this.$loading.fail()
        if (this.$loading.finish) this.$loading.finish()
      }
    },
    
    setLayout (layout) {
      if (!layout || !resolvedLayouts['_' + layout]) layout = 'default'
      this.layoutName = layout
      let _layout = '_' + layout
      this.layout = resolvedLayouts[_layout]
      return this.layout
    },
    loadLayout (layout) {
      if (!layout || !(layouts['_' + layout] || resolvedLayouts['_' + layout])) layout = 'default'
      let _layout = '_' + layout
      if (resolvedLayouts[_layout]) {
        return Promise.resolve(resolvedLayouts[_layout])
      }
      return layouts[_layout]()
      .then((Component) => {
        resolvedLayouts[_layout] = Component
        delete layouts[_layout]
        return resolvedLayouts[_layout]
      })
      .catch((e) => {
        if (this.$nuxt) {
          return this.$nuxt.error({ statusCode: 500, message: e.message })
        }
      })
    }
  },
  components: {
    NuxtLoading
  }
}

