'use strict'

import Vue from 'vue'
import Buefy from 'buefy'
import VueMoment from 'vue-moment'

import AppMain from '~/components/app-main'
import AppAuth from '~/components/app-auth'
import Toolbar from '~/components/toolbar'

export default async () => {
  Vue.component(AppMain.name, AppMain)
  Vue.component(AppAuth.name, AppAuth)
  Vue.component(Toolbar.name, Toolbar)

  Vue.use(Buefy)

  Vue.use(VueMoment)
}
