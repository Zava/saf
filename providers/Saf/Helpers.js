'use strict'

const { parse } = require('querystring')
const Logger = use('Logger')
const baseRules = require('indicative/builds/validations')

const Helpers = exports = module.exports = {}

Helpers.localize = (group, name, args = {}) => {
  const { formatMessage } = use('Antl')

  try {
    return formatMessage(`${group}.${name}`, args)
  } catch (err) {
    Logger.debug(`Cannot find translation for ${group}.${name}`)
    return false
  }
}

Helpers.validationMessages = () => {
  const formats = {}
  const rules = Object.keys(baseRules)

  /* istanbul ignore else */
  if (!rules.includes('unique')) {
    rules.push('unique')
  }

  for (let rule of rules) {
    rule = rule.split(/(?=[A-Z])/).join('_').toLowerCase()

    formats[rule] = (field, type, args) => {
      const params = { field, ...args }
      const trans = Helpers.localize('validations', `${field}.${type}`, params)

      if (trans) return trans

      return Helpers.localize('validations', type, params)
    }
  }

  return formats
}

/**
 * Determine param is a plain object.
 *
 * @param  {any}  obj  The object
 * @return {boolean}
 */
Helpers.isPlainObject = (obj) => {
  return !Array.isArray(obj) && (obj !== null && typeof obj === 'object')
}

/**
 * Parse sortable fields.
 *
 * The first argument accept `string` or `object`. If it recieving an object
 * any object's values will be normalize to `asd` or `desc` only and if the value
 * is falsy default value from second argument will be applied.
 *
 * @param  {string|object}  fields  Sortable field(s)
 * @param  {string}         order   Default ordering
 * @return {Array[]}
 *
 * @example ```js
 * parseSortable('key')
 *   // => [['key', 'desc']]
 * parseSortable('key:asc')
 *   // => [['key', 'asc']]
 * parseSortable('foo:asc;bar')
 *   // => [['foo', 'asc'],['bar','desc']]
 * parseSortable('foo:asc;bar:asc')
 *   // => [['foo', 'asc'],['bar','asc']]
 * parseSortable('foo:asc;bar:baz')
 *   // => [['foo', 'asc'],['bar','desc']]
 * ```
 */
Helpers.parseSortable = (fields, order) => {
  const terms = typeof fields === 'string' ? parse(fields, ';', ':') : fields

  for (const [key, value] of Object.entries(terms)) {
    // Make sure it has correct ordering, otherwise fallback to default
    if (!value || ['asc', 'desc'].indexOf(value) < 0) {
      terms[key] = order
    }
  }

  return Object.entries(terms)
}

/**
 * Parse searchable fields.
 *
 * @param  {string}  keyword  Searchable keyword(s)
 * @param  {string}  field    Default field
 * @return {Array[]}
 *
 * @example ```js
 * parseSearchable('key')
 *   // => [['id', 'key']]
 * parseSearchable('key:val')
 *   // => [['key', 'val']]
 * parseSearchable('key:one,two')
 *   // => [['key', ['one','two']]]
 * parseSearchable('key1:val1;key2')
 *   // => [['key', 'val'],['id','key2']]
 * parseSearchable('key1:val1;key2:val')
 *   // => [['key', 'val'],['key2','val']]
 * ```
 */
Helpers.parseSearchable = (keyword, field) => {
  const terms = typeof keyword === 'string' ? parse(keyword, ';', ':') : keyword

  for (const [key, value] of Object.entries(terms)) {
    if (!value) {
      terms[field] = key
      delete terms[key]
    }

    if (typeof value === 'string' && value.includes(',')) {
      terms[key] = value.split(',')
    }
  }

  return Object.entries(terms)
}

/**
 * Assign relation to object.
 *
 * @param  {object}  relations  Stack of relations
 * @param  {string}  relation   Relation name
 * @param  {string}  field      Related field
 * @param  {any}     value      Related terms
 */
Helpers.assignParsedRelations = (relations, relation, field, value) => {
  /* istanbul ignore else */
  if (typeof relations[relation] === 'undefined') {
    relations[relation] = {}
  }

  relations[relation][field] = value
}

/**
 * Parse relationable pairs by given params.
 *
 * @param  {any}  field  Field name
 * @param  {any}  value  The value
 * @return {string[]}
 */
Helpers.parseRelationable = (field, value) => {
  let related, column
  const [relation, ...rels] = field.split('.')

  /* istanbul ignore if */
  if (Helpers.isPlainObject(value)) {
    const [key, val] = Object.entries(value)[0]

    rels.push(key)
    value = val
  }

  column = rels.pop()
  rels.unshift(relation)

  related = rels.join('.')

  return [related, column, value]
}

Helpers.normalizeRequestFilter = (filter) => {
  return Object.assign({}, {
    includes: [],
    page: 0,
    limit: 10,
    sorting: {},
    search: {}
  }, filter)
}
