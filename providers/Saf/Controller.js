'use strict'

const { validateAll, sanitize, ValidationException } = use('Validator')
const Drive = use('Drive')
const { validationMessages } = require('./Helpers')

class Controller {
  /**
   * Validate input.
   *
   * @param  {object}   input              Input data object
   * @param  {object?}  validationRules    Validation rules
   * @param  {object?}  sanitizationRules  Satinization rules
   * @return {object}
   */
  async validate (input, validationRules, sanitizationRules) {
    const files = []
    const data = await this.getData(input, files)

    const rules = this.getValidationRules(validationRules)
    const validation = await validateAll(data, rules, this.getValidationMessage)

    if (validation.fails()) {
      throw ValidationException.validationFailed(validation.messages())
    }

    await Promise.all(files.map(field => this.processFile(field, data)))

    const sanitization = this.getSanitizationRules(sanitizationRules)

    if (sanitization) {
      return sanitize(data, sanitization)
    }

    return data
  }

  /**
   * Process the uploaded files.
   *
   * @param  {string}  field  Field name
   * @param  {object}  data   The data object
   * @return {object}
   */
  async processFile (field, data) {
    const { generate } = require('randomstring')
    const isMultiple = Array.isArray(data[field].files)
    const files = isMultiple ? data[field].files : [data[field]]
    const results = []

    await Promise.all(files.map(file => {
      const filePath = `${field}/${generate(10)}.${file.subtype}`

      results.push(filePath)

      return Drive.put(filePath, file.stream)
    }))

    data[field] = isMultiple ? results : results.shift()
    return data[field]
  }

  /**
   * Make sure we are getting correct data.
   *
   * @private
   * @param  {object}    input    Input data or instance of Request
   * @param  {string[]}  [files]  Files stack
   * @return {object}
   */
  async getData (input, files) {
    if (typeof input.all !== 'function') {
      return input
    }

    const data = this.hasFields ? input.only(this.constructor.fields) : input.all()

    for (const field of Object.keys(input._files)) {
      const file = input.file(field, {
        types: ['image'],
        size: '2mb'
      })

      data[field] = file
      files.push(field)
    }

    return data
  }

  /**
   * Make sure child controller has fields getter.
   *
   * @private
   * @return {boolean}
   */
  get hasFields () {
    return Array.isArray(this.constructor.fields) && !!this.constructor.fields.length
  }

  /**
   * @private
   * @param {object} rules Base validation rules
   * @return {object}
   */
  getValidationRules (rules) {
    return rules || this.constructor.validationRules || {}
  }

  /**
   * @private
   * @param {object} rules Base sanitization rules
   * @return {object|null}
   */
  getSanitizationRules (rules) {
    return rules || this.constructor.sanitizationRules || null
  }

  /**
   * Get validation message if any.
   *
   * @private
   * @return {object}
   */
  get getValidationMessage () {
    const messages = this.constructor.validationMessages || {}

    return Object.assign({}, validationMessages(), messages)
  }
}

module.exports = Controller
