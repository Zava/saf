'use strict'

const _ = require('lodash')

class SafSerializer {
  constructor (rows, pages = null, isOne = false) {
    this.rows = rows
    this.pages = pages
    this.isOne = isOne
  }

  /**
   * Attaches the eagerloaded relationship to the return
   * payload.
   *
   * @method _attachRelations
   * @private
   *
   * @param  {Model}   model   Model instance
   * @param  {Object}  output  Output
   */
  _attachRelations (model, output) {
    for (const [relation, values] of Object.entries(model.$relations)) {
      output[relation] = values && typeof (values.toJSON) === 'function' ? values.toJSON() : values
    }
  }

  /**
   * Attaches sideloaded data to the `__meta__` key on the
   * output object
   *
   * @method _attachMeta
   * @private
   *
   * @param  {Model}   model   Model instance
   * @param  {Object}  output  Output
   */
  _attachMeta (model, output) {
    if (_.size(model.$sideLoaded)) {
      output.__meta__ = _.clone(model.$sideLoaded)
    }
  }

  /**
   * Returns the json object for a given model instance
   *
   * @method _getRowJSON
   * @private
   *
   * @param  {Model}   model   Model instance
   * @return {Object}
   */
  _getRowJSON (model) {
    const json = model.toObject()
    this._attachRelations(model, json)
    this._attachMeta(model, json)

    return json
  }

  /**
   * Returns only the column names.
   *
   * @return {string[]}
   */
  columns () {
    const model = this.isOne ? this.rows : this.first()

    return Object.keys(model.toObject())
  }

  /**
   * Add row to the list of rows. Make sure the row
   * is an instance of the same model as the other
   * model instances.
   *
   * @method addRow
   *
   * @param  {Model}  row  Model instance
   */
  addRow (row) {
    this.rows.push(row)
  }

  /**
   * Get first model instance
   *
   * @method first
   *
   * @return {Model}
   */
  first () {
    return this.rows[0]
  }

  /**
   * Returns the row for the given index
   *
   * @method nth
   *
   * @param  {Number}  index  Index of models
   * @return {Model|Null}
   */
  nth (index) {
    return this.rows[index] || null
  }

  /**
   * Get last model instance
   *
   * @method last
   *
   * @return {Model}
   */
  last () {
    return this.rows[this.rows.length - 1]
  }

  /**
   * Returns the size of rows
   *
   * @method size
   *
   * @return {Number}
   */
  size () {
    return this.isOne ? 1 : this.rows.length
  }

  /**
   * Convert all rows/model instances to their JSON
   * representation
   *
   * @method toJSON
   *
   * @return {Array|Object}
   */
  toJSON () {
    if (this.isOne) {
      return this._getRowJSON(this.rows)
    }

    const data = this.rows.map(this._getRowJSON.bind(this))

    if (this.pages) {
      const pages = {
        total: Number(this.pages.total),
        shown: this.pages.perPage,
        limit: this.pages.page,
        last: this.pages.lastPage
      }

      return Object.assign({}, { pages, data })
    }

    return data
  }
}

module.exports = SafSerializer
