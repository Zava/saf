'use strict'

const relationParser = require('@adonisjs/lucid/src/Lucid/Relations/Parser')
const { ModelNotFoundException } = require('@adonisjs/lucid/src/Exceptions')
const Helpers = require('../Helpers')

const Lucid = use('Model')

/**
 * Base Model for Saf App.
 *
 * @class Model
 * @extends {Lucid}
 * @typedef {Lucid} SafModel
 */
class Model extends Lucid {
  /**
   * Default search key.
   *
   * Use primary key as default.
   *
   * @return {string}
   */
  /* istanbul ignore next */
  static get searchKey () {
    return this.constructor.primaryKey
  }

  static get Serializer () {
    return require('./Serializer')
  }

  /**
   * Default orderable key.
   *
   * Use primary key as default.
   *
   * @return {string}
   */
  static get orderKey () {
    return 'desc'
  }

  /**
   * Scope to join the relations required by filter.
   *
   * @private
   * @alias  joinRelation
   * @param  {object}  query     Instance of query builder
   * @param  {string}  relation  Relation name
   * @return {object}
   */
  static scopeJoinRelation (query, relation) {
    const model = new query.Model()

    relationParser.validateRelationExistence(model, relation)

    const relationInstance = relationParser.getRelatedInstance(model, relation)

    const {
      foreignKey, foreignTable,
      primaryKey, primaryTable
    } = relationInstance.relatedQuery.$relation

    const related = foreignTable !== relation
      ? `${foreignTable} as ${relation}`
      : relation

    return query.innerJoin(related, (qb) => {
      qb.on(`${relation}.${foreignKey}`, `${primaryTable}.${primaryKey}`)
    })
  }

  /**
   * Scope to sort the filter.
   *
   * @alias  sortBy
   * @param  {object}    query       Instance of query builder
   * @param  {object}    sorting     Object of sort
   * @param  {string[]}  [includes]  List of relations (if any)
   * @return {object}
   */
  static scopeSortBy (query, sorting, includes = []) {
    const relations = {}

    for (const [field, value] of Helpers.parseSortable(sorting, this.orderKey)) {
      if (field.includes('.') || Helpers.isPlainObject(value)) {
        const [name, key, val] = Helpers.parseRelationable(field, value)

        Helpers.assignParsedRelations(relations, name, key, val)

        continue
      }

      query.orderBy(field, value)
    }

    for (const [relation, terms] of Object.entries(relations)) {
      query.joinRelation(relation)

      /* istanbul ignore else */
      if (includes.indexOf(relation) < 0) {
        includes.push(relation)
      }

      for (const [key, value] of Object.entries(terms)) {
        query.orderBy(`${relation}.${key}`, value)
      }
    }

    return query
  }

  /**
   * Scope to search by keyword.
   *
   * @alias  searchBy
   * @param  {object}    query       Instance of query builder
   * @param  {object}    keyword     Object of search keywords
   * @param  {string[]}  [includes]  List of relations (if any)
   * @return {object}
   */
  static scopeSearchBy (query, keyword, includes = []) {
    const relations = {}

    for (const [field, value] of Helpers.parseSearchable(keyword, this.searchKey)) {
      if (field.includes('.') || Helpers.isPlainObject(value)) {
        const [name, key, val] = Helpers.parseRelationable(field, value)

        Helpers.assignParsedRelations(relations, name, key, val)

        continue
      }

      query.whereSearchable(field, value)
    }

    for (const [relation, terms] of Object.entries(relations)) {
      /* istanbul ignore else */
      if (includes.indexOf(relation) < 0) {
        includes.push(relation)
      }

      query.whereHas(relation, (qb) => {
        for (const [field, keyword] of Object.entries(terms)) {
          qb.whereSearchable(field, keyword)
        }
      })
    }

    return query
  }

  /**
   * Custom scope to handle case-insensitif where clause.
   *
   * This method only work on 'pgsql' as for now, I will fix it later on.
   * @link https://github.com/tgriesser/knex/issues/233
   *
   * @alias  whereSearchable
   * @param  {object}  query       Instance of query builder
   * @param  {string}  field       Field name
   * @param  {string}  keyword     Where keyword
   * @param  {string}  [operator]  Where operator
   * @return {object}
   *
   * @example ```js
   * query.whereSearchable('field', 'keyword')
   *   // => query.where('field', 'ilike', 'keyword')
   * query.whereSearchable('field', 'start..end')
   *   // => query.whereBetween('field', ['start', 'end'])
   * query.whereSearchable('field', ['one', 'two'])
   *   // => query.whereIn('field', ['one', 'two'])
   * ```
   */
  static scopeWhereSearchable (query, field, keyword, operator = 'ilike') {
    if (Array.isArray(keyword)) {
      return query.whereIn(field, keyword)
    }

    if (/[.]{2}/.test(keyword)) {
      const [start, end] = keyword.split('..').map(k => {
        return k.replace(/^\.+|\.+$/g, '')
      })

      return query.whereBetween(field, [start, end])
    }

    keyword = keyword.toLowerCase()

    return query.where(field, operator, `%${keyword}%`)
  }

  /**
   * Scope to filter the required data.
   *
   * @alias  filter
   * @param  {object}   query             Instance of query builder
   * @param  {object}   terms             Main filter object
   * @param  {object}   [terms.sorting]   Object of sortable fields
   * @param  {object}   [terms.search]    Object of searchable keywords
   * @param  {string[]} [terms.includes]  Relation name should be loaded
   * @return {object}
   *
   * @example ```js
   * const data = await ModelClass.query().filter(...filterObj).fetch()
   * ```
   */
  static scopeFilter (query, { sorting, search, includes = [] }) {
    if (!Array.isArray(includes)) {
      includes = []
    }

    /* istanbul ignore else */
    if (sorting) {
      query.sortBy(sorting, includes)
    }

    /* istanbul ignore else */
    if (search) {
      query.searchBy(search, includes)
    }

    for (const include of includes) {
      query.with(include)
    }

    return query
  }

  /**
   * Sortcut to filtering data.
   *
   * @param  {object}    terms             Filter terms
   * @param  {Number}    terms.page        Page number (default: 1)
   * @param  {Number}    terms.limit       Per-page limit (default: 10)
   * @param  {string[]}  [terms.includes]  Relation name should be loaded
   * @param  {object}    [terms.sorting]   Object of sortable fields
   * @param  {object}    [terms.search]    Object of searchable keywords
   * @param  {Function}  [callback]        Just in case need additional query
   * @return {object}
   * @throws ModelNotFoundException
   *
   * @example ```js
   * const data = await ModelClass.filterBy(...filterObj)
   * ```
   */
  static async filterBy ({ page, limit, ...terms }, callback) {
    const query = this.query()

    if (typeof callback === 'function') {
      await callback(query)
    }

    // Normalize if filter is empty
    const filter = Object.assign({
      includes: [],
      sorting: {},
      search: {}
    }, terms)

    const model = await query.filter(filter).paginate(page || 1, limit || 10)

    if (model.size() === 0) {
      throw ModelNotFoundException.raise(this.name)
    }

    return model
  }
}

module.exports = Model
