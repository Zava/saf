'use strict'


const ValidatorRules = exports = module.exports = {}

ValidatorRules.number = async (data, field, message, args, get) => {
  const value = get(data, field)

  /* istanbul ignore if */
  if (!value) return

  if (!/^[0-9]+$/g.test(value)) {
    throw message
  }
}

ValidatorRules.phone = async (data, field, message, args, get) => {
  const value = get(data, field)

  /* istanbul ignore if */
  if (!value) return

  if (!/^[0-9]+$/g.test(value) || (value.length < 7 || value.length > 16)) {
    throw message
  }
}

ValidatorRules.file = async (data, field, message, args, get) => {
  const value = get(data, field)

  /* istanbul ignore if */
  if (!value) return

  const errors = []
  const files = Array.isArray(value.files) ? value.files : [value]

  files.map(file => {
    file._validateFile()

    if (Object.keys(file._error).length) {
      errors.push(file._error)
    }
  })

  if (errors.length > 0) {
    throw errors
  }
}
