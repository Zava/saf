'use strict'

const { ServiceProvider } = require('@adonisjs/fold')

class SafProvider extends ServiceProvider {
  register () {
    this.app.singleton('Saf/Model', () => require('./Model'))
    this.app.singleton('Saf/Model/Serializer', () => require('./Model/Serializer'))

    this.app.singleton('Saf/Controller', () => require('./Controller'))
    this.app.singleton('Saf/Helpers', () => require('./Helpers'))

    this.app.singleton('Setting', (app) => {
      const Model = app.use('App/Models/Setting')
      const Setting = app.use('App/Services/Setting')

      return new Setting(Model)
    })

    // this.app.singleton('Saf/AuthSerializer', () => require('./AuthSerializer'))

    if (process.env.NODE_ENV === 'testing') {
      this.registerTestingTrait()
    }
  }

  async boot () {
    await this.bootSettings()

    this.addRequestMacros(this.app)

    this.extendValidatorRules()

    // this.extendAuthSerializer()

    this.addGlobalViews(this.app)
  }

  addRequestMacros (app) {
    const Request = app.use('Adonis/Src/Request')
    const Route = app.use('Adonis/Src/Route')
    const { normalizeRequestFilter } = require('./Helpers')

    Request.macro('filter', function () {
      const query = normalizeRequestFilter(this.get())

      query.load = (...params) => {
        query.includes = params
      }

      return query
    })

    Request.macro('route', function () {
      return Route.list().find(({ verbs, _regexp }) => {
        return verbs.includes(this.method()) && _regexp.test(this.url())
      })
    })
  }

  addGlobalViews (app) {
    const View = app.use('Adonis/Src/View')
    const Config = app.use('Adonis/Src/Config')
    const Route = app.use('Adonis/Src/Route')

    View.global('project', Config.get('project'))

    View.global('fullRoute', (...args) => {
      return `${process.env.APP_URL}/${Route.url(...args).replace(/^\/*/, '')}`
    })
  }

  async bootSettings () {
    const Helpers = this.app.use('Adonis/Src/Helpers')

    // console.log('Command', process.argv.slice(2)[0] || '')
    if (Helpers.isAceCommand()) return

    const Setting = this.app.use('Setting')
    const HttpContext = this.app.use('Adonis/Src/HttpContext')

    await Setting.boot()

    HttpContext.getter('settings', () => {
      return Setting
    }, true)
  }

  extendValidatorRules () {
    const Validator = this.app.use('Adonis/Addons/Validator')
    const ValidatorRules = require('./ValidatorRules')

    Validator.extend('phone', ValidatorRules.phone)

    Validator.extend('number', ValidatorRules.number)

    Validator.extend('file', ValidatorRules.file)
  }

  extendAuthSerializer () {
    const Auth = this.app.use('Adonis/Src/Auth')

    Auth.extend('saf', 'Saf/AuthSerializer', 'serializer')
  }

  registerTestingTrait () {
    this.app.bind('Saf/Test', (app) => {
      const Chance = require('chance')
      const Config = app.use('Config')

      return ({ Context }, options = {}) => {
        const opt = Object.assign({
          mock: undefined,
          disableCsrf: false,
          endPoint: undefined
        }, options)

        // Disable csrf while testing
        if (opt.disableCsrf === true) {
          Config.set('shield.csrf.enable', false)
        }

        Context.getter('fake', () => new Chance())

        Context.getter('project', () => Config.get('project'))

        Context.getter('fixtures', () => {
          return require('../../database/fixtures')
        })

        if (opt.endPoint) {
          Context.getter('endPoint', () => opt.endPoint)
        }

        if (opt.mock) {
          if (opt.mock.includes('Models')) {
            Context.getter('Model', () => app.use(opt.mock))

            Context.getter('Factory', () => {
              return app.use('Factory').model(opt.mock)
            })
          } else {
            Context.getter('Mock', () => app.use(opt.mock))
          }
        }
      }
    })
  }
}

module.exports = SafProvider
