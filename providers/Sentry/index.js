'use strict'

const Raven = require('raven')

class Sentry {
  constructor (dsn, options = {}) {
    Raven.config(dsn, options).install((error, initialErr, eventId) => {
      if (error) {
        console.error(error) // eslint-disable-line no-console
      }
    })
  }

  makeContext (request, auth) {
    const context = {
      request: request.request
    }

    if (auth.user) {
      context.user = {
        id: auth.user.primaryKeyValue,
        email: auth.user.email
      }
    }

    return context
  }

  capture (e, request, auth) {
    const context = this.makeContext(request, auth)

    return new Promise((resolve, reject) => {
      return Raven.captureException(e, context, (error, eventId) => {
        if (error) {
          return reject(error)
        }

        return resolve(eventId)
      })
    })
  }

  get newClient () {
    return new Raven.Client()
  }
}

module.exports = Sentry
