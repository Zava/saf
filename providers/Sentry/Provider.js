'use strict'

const { ServiceProvider } = require('@adonisjs/fold')

class SentryProvider extends ServiceProvider {
  register () {
    this.app.singleton('Sentry/Config', (app) => {
      const { version } = require('../../package.json')
      const Config = app.use('Adonis/Src/Config')
      const Env = app.use('Adonis/Src/Env')

      const { dsn, options: userOptions } = Config.get('sentry')

      /** @link https://docs.sentry.io/clients/node/config/ */
      const baseOptions = {
        release: Env.get('HEROKU_SLUG_COMMIT', version),
        captureUnhandledRejections: true,
        tags: {}
      }

      if (Env.get('NODE_ENV') !== 'production') {
        baseOptions.release = this.git.commit
        baseOptions.tags.git_branch = this.git.branch
      }

      const publicDsn = this.getPublicDsn(dsn)
      const options = Object.assign(baseOptions, userOptions)

      return { dsn, publicDsn, options }
    })

    this.app.singleton('Sentry/Client', (app) => {
      const { dsn, options } = app.use('Sentry/Config')

      return new (require('.'))(dsn, options)
    })
  }

  getPublicDsn (dsn) {
    const { URL } = require('url')
    const uri = new URL(dsn)

    uri.password = ''

    return uri.href
  }

  get git () {
    const git = require('git-rev-sync')

    try {
      return {
        branch: git.branch(),
        commit: git.long()
      }
    } catch (error) {
      console.error(error) // eslint-disable-line no-console
    }
  }
}

module.exports = SentryProvider
