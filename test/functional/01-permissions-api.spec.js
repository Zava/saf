'use strict'

const { test, trait } = use('Test/Suite')('Permissions API')
const { getUserToken } = require('../util')

trait('Test/ApiClient')
trait('Auth/Client')
trait('Saf/Test', {
  mock: 'App/Models/Permission',
  endPoint: '/permissions'
})

test('Fetch Permissions', async ({ client, assert, endPoint }) => {
  const apiToken = await getUserToken()
  const response = await client.get(endPoint)
    .accept('json')
    .loginVia(apiToken)
    .end()

  response.assertStatus(200)
  assert.equal(8, response.body.pages.total)
})

test('Create Permission', async ({ client, endPoint, Factory }) => {
  const input = await Factory.make()
  const apiToken = await getUserToken()
  const response = await client.post(endPoint)
    .accept('json')
    .loginVia(apiToken)
    .send(input.toJSON())
    .end()

  response.assertStatus(201)
  response.assertJSONSubset({
    model: input.toJSON()
  })
})

test('Show Permission', async ({ client, endPoint, Model }) => {
  const item = await Model.last()
  const apiToken = await getUserToken()
  const response = await client.get(`${endPoint}/${item.id}`)
    .accept('json')
    .loginVia(apiToken)
    .end()

  response.assertStatus(200)
  response.assertJSONSubset({
    model: item.toJSON()
  })
})

test('Update Permission', async ({ client, endPoint, Model, Factory }) => {
  const input = await Factory.make()
  const model = await Model.last()
  const apiToken = await getUserToken()
  const response = await client.put(`${endPoint}/${model.id}`)
    .accept('json')
    .loginVia(apiToken)
    .send(input.toJSON())
    .end()

  response.assertStatus(200)
  response.assertJSONSubset({
    model: input.toJSON()
  })
})

test('Delete Permission', async ({ client, endPoint, Model }) => {
  const item = await Model.last()
  const apiToken = await getUserToken()
  const response = await client.delete(`${endPoint}/${item.id}`)
    .accept('json')
    .loginVia(apiToken)
    .end()

  response.assertStatus(200)
})
