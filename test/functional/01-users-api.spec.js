'use strict'

const Event = use('Event')
const { test, trait } = use('Test/Suite')('Users API')
const { getUserToken } = require('../util')

trait('Test/ApiClient')
trait('Auth/Client')
trait('Saf/Test', {
  mock: 'App/Models/User',
  endPoint: '/users'
})

test('Fetch Users', async ({ client, assert, endPoint, Factory }) => {
  const apiToken = await getUserToken()
  const response = await client.get(endPoint)
    .accept('json')
    .loginVia(apiToken)
    .end()

  response.assertStatus(200)
  assert.equal(3, response.body.pages.total)
})

test('Create User', async ({ client, endPoint, fake }) => {
  const input = {
    email: fake.email()
  }

  Event.fake()

  const apiToken = await getUserToken()
  const response = await client.post(endPoint)
    .accept('json')
    .loginVia(apiToken)
    .send(input)
    .end()

  const { token } = Event.pullRecent().data[0]

  response.assertStatus(201)
  response.assertJSONSubset({
    model: {
      email: input.email
    }
  })

  const join = await client.get('account/confirm/invitation')
    .query({ token })
    .end()

  join.assertStatus(200)

  Event.restore()
})

test('Show Users', async ({ client, endPoint, Model }) => {
  const item = await Model.last()
  const apiToken = await getUserToken()
  const response = await client.get(`${endPoint}/${item.id}`)
    .accept('json')
    .loginVia(apiToken)
    .end()

  response.assertStatus(200)
  response.assertJSONSubset({
    model: item.toJSON()
  })
})

test('Update User', async ({ client, endPoint, Model, Factory }) => {
  const model = await Factory.create()
  const apiToken = await getUserToken()

  const halt = await client.put(`${endPoint}/${model.id}`)
    .accept('json')
    .loginVia(apiToken)
    .send({ activate: true })
    .end()

  halt.assertStatus(200, 'User is now activated')
  halt.assertJSONSubset({
    model: { is_active: true }
  })

  const ban = await client.put(`${endPoint}/${model.id}`)
    .accept('json')
    .loginVia(apiToken)
    .send({ ban: true, reason: 'You are ugly' })
    .end()

  ban.assertStatus(200, 'User is now banned')
  ban.assertJSONSubset({
    model: { is_banned: true, ban_reason: 'You are ugly' }
  })
})

test('Delete User', async ({ client, endPoint, Model }) => {
  const item = await Model.last()
  const apiToken = await getUserToken()
  const response = await client.delete(`${endPoint}/${item.id}`)
    .accept('json')
    .loginVia(apiToken)
    .end()

  response.assertStatus(200)
})
