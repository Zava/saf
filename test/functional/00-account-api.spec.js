'use strict'

const Event = use('Event')
const Antl = use('Antl')
const { test, trait } = use('Test/Suite')('Account API')
const { getUserToken } = require('../util')

trait('Test/ApiClient')
trait('Auth/Client')
trait('Saf/Test', {
  mock: 'App/Models/User'
})

test('Register with incorrect data', async ({ assert, client, project }) => {
  const usernameUnique = {
    message: Antl.formatMessage('validations.unique'),
    validation: 'unique'
  }
  const emailUnique = {
    message: Antl.formatMessage('validations.unique'),
    validation: 'unique'
  }
  const emailRequired = {
    message: Antl.formatMessage('validations.required'),
    validation: 'required'
  }
  const passwordConfirmed = {
    message: Antl.formatMessage('validations.confirmed'),
    validation: 'confirmed'
  }
  const passwordRequired = {
    message: Antl.formatMessage('validations.required'),
    validation: 'required'
  }

  const userExists = await client.post('account/register')
    .accept('json')
    .send({ username: 'admin', password: 'secret' })
    .end()

  userExists.assertJSONSubset({
    fields: {
      username: [usernameUnique],
      email: [emailRequired],
      password: [passwordConfirmed]
    }
  })

  const emailExists = await client.post('account/register')
    .accept('json')
    .send({ username: 'john', email: 'admin@getsaf.app' })
    .end()

  emailExists.assertJSONSubset({
    fields: {
      // username: [usernameUnique],
      email: [emailUnique],
      password: [passwordRequired]
    }
  })
})

test('Register with correct data', async ({ assert, client }) => {
  const credential = {
    username: 'john-doe',
    email: 'johndoe@example.com',
    password: 'secret',
    password_confirmation: 'secret'
  }

  Event.fake()

  const register = await client.post('account/register')
    .accept('json')
    .send(credential)
    .end()

  const { token } = Event.pullRecent().data[0]

  register.assertStatus(201, 'Registering new user')

  const confirm = await client.get(`account/confirm/activation`)
    .query({ token })
    .end()

  const { type } = Event.pullRecent().data[0]

  confirm.assertStatus(201, 'Confirm new user account')
  assert.equal(type, 'activation')

  Event.restore()
}).timeout(0)

test('Login with unregistered credential', async ({ assert, client }) => {
  const credential = { username: 'johndoe', password: 'foobar' }
  const res = await client.post('account/login')
    .accept('json')
    .send(credential)
    .end()

  res.assertStatus(401)
})

test('Login with inactivated credential', async ({ assert, client }) => {
  const credential = { username: 'johndoe', password: 'foobar' }
  const res = await client.post('account/login')
    .accept('json')
    .send(credential)
    .end()

  res.assertStatus(401)
})

test('Login with banned credential', async ({ assert, client }) => {
  const credential = { username: 'johndoe', password: 'foobar' }
  const res = await client.post('account/login')
    .accept('json')
    .send(credential)
    .end()

  res.assertStatus(401)
})

test('Login with valid credential', async ({ assert, client }) => {
  const credential = { username: 'john-doe', password: 'secret' }
  const login = await client.post('account/login')
    .accept('json')
    .send(credential)
    .end()

  // console.log(login)
  login.assertStatus(200)
})

test('Show User Account', async ({ assert, client, Factory }) => {
  const apiToken = await getUserToken('john-doe')
  const showAccount = await client.get('account')
    .accept('json')
    .loginVia(apiToken)
    .end()

  showAccount.assertStatus(200)
})

test('Update User Account', async ({ assert, client, Factory }) => {
  const apiToken = await getUserToken('john-doe')
  const user = {
    username: 'john-due',
    email: 'johndoe@example.com'
  }

  const changeUsername = await client.put('account')
    .accept('json')
    .loginVia(apiToken)
    .send(user)
    .end()

  changeUsername.assertStatus(200)
  changeUsername.assertJSONSubset({ account: user })

  user.email = 'johndoe@example.me'
  user.password = '123234'
  user.password_confirmation = '123234'

  Event.fake()

  const changeEmail = await client.put('account')
    .accept('json')
    .loginVia(apiToken)
    .send(user)
    .end()

  const { token } = Event.pullRecent().data[0]

  changeEmail.assertStatus(200)

  const confirm = await client.get(`account/confirm/update-email`)
    .query({ token })
    .end()

  const { type } = Event.pullRecent().data[0]

  confirm.assertStatus(200, 'Confirm new user account')
  assert.equal(type, 'update-email')

  Event.restore()
})

test('Logout by unauthenticated account', async ({ assert, client }) => {
  const res = await client.post('account/logout')
    .accept('json')
    .end()

  res.assertStatus(401)
  res.assertJSONSubset({
    message: 'Token autentikasi tidak valid'
  })
})

test('Logout by authenticated account', async ({ assert, client }) => {
  const apiToken = await getUserToken('john-due')
  const res = await client.post('account/logout')
    .accept('json')
    .loginVia(apiToken)
    .end()

  res.assertStatus(200)
})

test('Request reset password', async ({ assert, client }) => {
  const account = { credential: 'john-due' }

  Event.fake()

  const requestPassword = await client.post('account/forgot')
    .accept('json')
    .send(account)
    .end()

  const { token } = Event.pullRecent().data[0]

  requestPassword.assertStatus(200)

  const confirm = await client.get('account/confirm/update-password')
    .query({ token })
    .end()

  confirm.assertStatus(200)

  Event.restore()
})

test('Confirm request with invalid token', async ({ client }) => {
  const confirm = await client.get('account/confirm/update-password')
    .query({ token: 'invalid-token-string' })
    .end()

  confirm.assertStatus(401)
})

test('Accessing unauthorized routes', async ({ client, Factory }) => {
  const user = await Factory.create()
  const apiToken = await getUserToken(user.username)
  const noroles = await client.get('/people')
    .accept('json')
    .loginVia(apiToken)
    .end()

  noroles.assertStatus(401)

  const role = await use('App/Services/Account').getDefaultRole()
  await user.roles().attach([role.id])

  const nopermissions = await client.get('/people')
    .accept('json')
    .loginVia(apiToken)
    .end()

  nopermissions.assertStatus(401)
})
