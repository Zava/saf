'use strict'

const Helpers = use('Helpers')
const { test, trait, before } = use('Test/Suite')('Settings API')
const { getUserToken } = require('../util')

trait('Test/ApiClient')
trait('Auth/Client')
trait('Saf/Test', {
  mock: 'App/Models/Setting',
  endPoint: '/settings'
})

before(async () => {
  await use('Setting').boot()
})

test('Fetch Settings', async ({ client, assert, endPoint }) => {
  const apiToken = await getUserToken()
  const response = await client.get(endPoint)
    .accept('json')
    .loginVia(apiToken)
    .end()

  response.assertStatus(200)
})

test('Update Settings', async ({ client, endPoint, Model }) => {
  const settings = await Model.all()
  const apiToken = await getUserToken()
  const request = client.put(endPoint).loginVia(apiToken)

  for (const { slug, payload } of settings.toJSON()) {
    if (slug === 'site_logo') {
      request.attach(slug, Helpers.databasePath('fixtures/logo.png'))
    } else {
      const value = slug === 'company_name' ? 'ACME Inc.' : payload.value

      request.field(slug, value)
    }
  }

  const response = await request.end()

  response.assertStatus(200)
})
