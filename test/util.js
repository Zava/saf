'use strict'

const User = use('App/Models/User')
const Encrypt = use('Adonis/Src/Encryption')

exports.getUserToken = async (username = 'admin', type = 'api_token') => {
  const user = await User.query().where({ username })
    .whereHas('tokens', (qb) => {
      qb.where({ type, is_revoked: false })
    }).with('tokens').first()

  if (user === null) return User.findBy('username', username)

  const auth = user.getRelated('tokens').toJSON().find(t => !t.is_revoked)

  return auth ? Encrypt.encrypt(auth.token) : user
}
