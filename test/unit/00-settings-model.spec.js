'use strict'

const { test, trait } = use('Test/Suite')('Settings Model')

trait('Saf/Test', {
  mock: 'App/Models/Setting'
})

test('Default type should be a string', async ({ assert, Model }) => {
  const setting = await Model.create({
    slug: 'foo.setting',
    name: 'foo.setting',
    payload: { type: 'bar', value: 'Foo Bar' }
  })

  assert.equal(setting.toJSON().type, 'string')
})

test('Return correct value by type', async ({ assert, Model }) => {
  let json
  const setting = await Model.create({
    slug: 'bar.setting',
    name: 'bar.setting',
    payload: { type: 'array', value: ['Foo', 'Bar'] }
  })

  json = setting.toJSON()
  assert.isArray(json.value)
  assert.deepEqual(json.value, ['Foo', 'Bar'])

  setting.payload = { type: 'object', value: { foo: 'Bar' } }

  await setting.save()

  json = setting.toJSON()
  assert.isObject(json.value)
  assert.deepEqual(json.value, { foo: 'Bar' })

  setting.payload = { type: 'number', value: '20' }

  await setting.save()

  json = setting.toJSON()
  assert.equal(json.value, 20)
})
