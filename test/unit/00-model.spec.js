'use strict'

const { test, trait } = use('Test/Suite')('Base Model')

trait('Saf/Test', {
  mock: 'App/Models/User'
})

test('Model.sortBy accept string parameters', async ({ assert, Model }) => {
  let expected, actual

  actual = Model.query().sortBy('name').toSQL()
  expected = Model.query().orderBy('name', 'desc').toSQL()

  assert.equal(actual.sql, expected.sql, 'Order by string field name')

  actual = Model.query().sortBy('name:asc').toSQL()
  expected = Model.query().orderBy('name', 'asc').toSQL()

  assert.equal(actual.sql, expected.sql, 'Order by string field and value')

  actual = Model.query().sortBy('name:asc;address').toSQL()
  expected = Model.query().orderBy('name', 'asc').orderBy('address', 'desc').toSQL()

  assert.equal(actual.sql, expected.sql, 'Order by string fields that one has value')

  actual = Model.query().sortBy('name:asc;address:asc').toSQL()
  expected = Model.query().orderBy('name', 'asc').orderBy('address', 'asc').toSQL()

  assert.equal(actual.sql, expected.sql, 'Order by string multiple fields with value')
})

test('Model.sortBy accept object parameters', async ({ assert, Model }) => {
  let expected, actual

  actual = Model.query().sortBy({ name: null }).toSQL()
  expected = Model.query().orderBy('name', 'desc').toSQL()

  assert.equal(actual.sql, expected.sql, 'Order by object field name')

  actual = Model.query().sortBy({ name: 'asc' }).toSQL()
  expected = Model.query().orderBy('name', 'asc').toSQL()

  assert.equal(actual.sql, expected.sql, 'Order by object field and value')

  actual = Model.query().sortBy({ name: 'asc', address: null }).toSQL()
  expected = Model.query().orderBy('name', 'asc').orderBy('address', 'desc').toSQL()

  assert.equal(actual.sql, expected.sql, 'Order by object fields that one has value')

  actual = Model.query().sortBy({ name: 'asc', address: 'asc' }).toSQL()
  expected = Model.query().orderBy('name', 'asc').orderBy('address', 'asc').toSQL()

  assert.equal(actual.sql, expected.sql, 'Order by object multiple fields with value')
})

test('Model.sortBy accept string with relation parameters', async ({ assert, Model }) => {
  let expected, actual

  actual = Model.query().sortBy('profile.name').toSQL()
  expected = Model.query().innerJoin('people as profile', (qb) => {
    qb.on('profile.user_id', 'users.id')
  }).orderBy('profile.name', 'desc').toSQL()

  assert.equal(actual.sql, expected.sql, 'Order by string field name')

  actual = Model.query().sortBy('profile.name:asc').toSQL()
  expected = Model.query().innerJoin('people as profile', (qb) => {
    qb.on('profile.user_id', 'users.id')
  }).orderBy('profile.name', 'asc').toSQL()

  assert.equal(actual.sql, expected.sql, 'Order by string field and value')

  actual = Model.query().sortBy('profile.name:asc;address').toSQL()
  expected = Model.query().innerJoin('people as profile', (qb) => {
    qb.on('profile.user_id', 'users.id')
  }).orderBy('address', 'desc').orderBy('profile.name', 'asc').toSQL()

  assert.equal(actual.sql, expected.sql, 'Order by string fields that one has value')

  actual = Model.query().sortBy('profile.name:asc;address:asc').toSQL()
  expected = Model.query().innerJoin('people as profile', (qb) => {
    qb.on('profile.user_id', 'users.id')
  }).orderBy('address', 'asc').orderBy('profile.name', 'asc').toSQL()

  assert.equal(actual.sql, expected.sql, 'Order by string multiple fields with value')
})

test('Model.sortBy accept object with relation parameters', async ({ assert, Model }) => {
  let expected, actual

  actual = Model.query().sortBy({ 'profile.name': null }).toSQL()
  expected = Model.query().innerJoin('people as profile', (qb) => {
    qb.on('profile.user_id', 'users.id')
  }).orderBy('profile.name', 'desc').toSQL()

  assert.equal(actual.sql, expected.sql, 'Order by string field name')

  actual = Model.query().sortBy({ 'profile.name': 'asc' }).toSQL()
  expected = Model.query().innerJoin('people as profile', (qb) => {
    qb.on('profile.user_id', 'users.id')
  }).orderBy('profile.name', 'asc').toSQL()

  assert.equal(actual.sql, expected.sql, 'Order by string field and value')

  actual = Model.query().sortBy({ 'profile.name': 'asc', address: null }).toSQL()
  expected = Model.query().innerJoin('people as profile', (qb) => {
    qb.on('profile.user_id', 'users.id')
  }).orderBy('address', 'desc').orderBy('profile.name', 'asc').toSQL()

  assert.equal(actual.sql, expected.sql, 'Order by string fields that one has value')

  actual = Model.query().sortBy({ 'profile.name': 'asc', address: 'asc' }).toSQL()
  expected = Model.query().innerJoin('people as profile', (qb) => {
    qb.on('profile.user_id', 'users.id')
  }).orderBy('address', 'asc').orderBy('profile.name', 'asc').toSQL()

  assert.equal(actual.sql, expected.sql, 'Order by string multiple fields with value')
})

test('Model.searchBy accept string parameters', async ({ assert, Model }) => {
  let expected, actual

  actual = Model.query().searchBy('value').toSQL()
  expected = Model.query().where('username', 'ilike', 'value').toSQL()

  assert.equal(actual.sql, expected.sql, 'Search by string value')

  actual = Model.query().searchBy('name:value').toSQL()
  expected = Model.query().where('name', 'ilike', 'value').toSQL()

  assert.equal(actual.sql, expected.sql, 'Search by string field and value')

  actual = Model.query().searchBy('name:one,two').toSQL()
  expected = Model.query().whereIn('name', ['one', 'two']).toSQL()

  assert.equal(actual.sql, expected.sql, 'Search by string field and arrayale value')

  actual = Model.query().searchBy('name:start..end').toSQL()
  expected = Model.query().whereBetween('name', ['start', 'end']).toSQL()

  assert.equal(actual.sql, expected.sql, 'Search by string field and between value')

  actual = Model.query().searchBy('name:foo;bar').toSQL()
  expected = Model.query().where('name', 'ilike', 'foo').where('username', 'ilike', 'bar').toSQL()

  assert.equal(actual.sql, expected.sql, 'Search by string fields that one has value')

  actual = Model.query().searchBy('name:foo;address:bar').toSQL()
  expected = Model.query().where('name', 'ilike', 'foo').where('address', 'ilike', 'bar').toSQL()

  assert.equal(actual.sql, expected.sql, 'Search by string multiple fields with value')
})

test('Model.searchBy accept object parameters', async ({ assert, Model }) => {
  let expected, actual

  actual = Model.query().searchBy({ value: null }).toSQL()
  expected = Model.query().where('username', 'ilike', 'value').toSQL()

  assert.equal(actual.sql, expected.sql, 'Search by string value')

  actual = Model.query().searchBy({ name: 'value' }).toSQL()
  expected = Model.query().where('name', 'ilike', 'value').toSQL()

  assert.equal(actual.sql, expected.sql, 'Search by string field and value')

  actual = Model.query().searchBy({ name: 'foo', bar: null }).toSQL()
  expected = Model.query().where('name', 'ilike', 'foo').where('username', 'ilike', 'bar').toSQL()

  assert.equal(actual.sql, expected.sql, 'Search by string fields that one has value')

  actual = Model.query().searchBy({ name: 'foo', address: 'bar' }).toSQL()
  expected = Model.query().where('name', 'ilike', 'foo').where('address', 'ilike', 'bar').toSQL()

  assert.equal(actual.sql, expected.sql, 'Search by string multiple fields with value')
})

test('Model.searchBy accept string with relation parameters', async ({ assert, Model }) => {
  let expected, actual

  actual = Model.query().searchBy('profile.name:value').toSQL()
  expected = Model.query().whereHas('profile', (qb) => {
    qb.where('name', 'ilike', 'value')
  }).toSQL()

  assert.equal(actual.sql, expected.sql, 'Search by string field and value')

  actual = Model.query().searchBy('profile.name:one,two').toSQL()
  expected = Model.query().whereHas('profile', (qb) => {
    qb.whereIn('name', ['one', 'two'])
  }).toSQL()

  assert.equal(actual.sql, expected.sql, 'Search by string field and array value')

  actual = Model.query().searchBy('profile.name:foo;bar').toSQL()
  expected = Model.query().where('username', 'ilike', 'bar')
    .whereHas('profile', (qb) => {
      qb.where('name', 'ilike', 'foo')
    }).toSQL()

  assert.equal(actual.sql, expected.sql, 'Search by string fields that one has value')

  actual = Model.query().searchBy('profile.name:foo;address:bar').toSQL()
  expected = Model.query().where('address', 'ilike', 'bar')
    .whereHas('profile', (qb) => {
      qb.where('name', 'ilike', 'foo')
    }).toSQL()

  assert.equal(actual.sql, expected.sql, 'Search by string multiple fields with value')
})

test('Model.searchBy accept object with relation parameters', async ({ assert, Model }) => {
  let expected, actual

  actual = Model.query().searchBy({ 'profile.name': 'value' }).toSQL()
  expected = Model.query().whereHas('profile', (qb) => {
    qb.where('name', 'ilike', 'value')
  }).toSQL()

  assert.equal(actual.sql, expected.sql, 'Search by string field and value')

  actual = Model.query().searchBy({ 'profile.name': 'foo', bar: null }).toSQL()
  expected = Model.query().where('username', 'ilike', 'bar')
    .whereHas('profile', (qb) => {
      qb.where('name', 'ilike', 'foo')
    }).toSQL()

  assert.equal(actual.sql, expected.sql, 'Search by string fields that one has value')

  actual = Model.query().searchBy({ 'profile.name': 'foo', address: 'bar' }).toSQL()
  expected = Model.query().where('address', 'ilike', 'bar')
    .whereHas('profile', (qb) => {
      qb.where('name', 'ilike', 'foo')
    }).toSQL()

  assert.equal(actual.sql, expected.sql, 'Search by string multiple fields with value')
})

test('Model.filter', async ({ assert, Model }) => {
  let expected, actual

  actual = Model.query().filter({
    sorting: 'name',
    search: 'name:john'
  }).toSQL()
  expected = Model.query()
    .where('name', 'ilike', 'john')
    .orderBy('name', 'desc')
    .toSQL()

  assert.equal(actual.sql, expected.sql, 'Filter data')
})
