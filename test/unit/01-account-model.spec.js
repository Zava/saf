'use strict'

const { test } = use('Test/Suite')('Account Models')
const User = use('App/Models/User')

test('Get User', async ({ assert }) => {
  const user = await User.first()

  await user.loadMany(['roles', 'permissions'])

  assert.equal(1, user.getRelated('roles').size())
  assert.equal(0, user.getRelated('permissions').size())
})
