'use strict'

const { test } = use('Test/Suite')('Custom Validator rules')
const { validate } = use('Validator')

test('Validator.number', async ({ assert }) => {
  let validation
  const rules = { field: 'number' }

  validation = await validate({ field: 'string' }, rules)

  assert.isTrue(validation.fails(), 'Do not accept string param')

  validation = await validate({ field: '123' }, rules)

  assert.isFalse(validation.fails(), 'Accept numeric param')
})

test('Validator.phone', async ({ assert }) => {
  let validation
  const rules = { field: 'phone' }

  validation = await validate({ field: 'stringparam' }, rules)

  assert.isTrue(validation.fails(), 'Do not accept string param')

  validation = await validate({ field: '123' }, rules)

  assert.isTrue(validation.fails(), 'Do not accept numeric less than 7 characters')

  validation = await validate({ field: '1234567' }, rules)

  assert.isFalse(validation.fails(), 'Accept param with minimum of 7 chars')

  validation = await validate({ field: '1234567890123456' }, rules)

  assert.isFalse(validation.fails(), 'Accept param with maximum of 16 chars')

  validation = await validate({ field: '12345678901234567' }, rules)

  assert.isTrue(validation.fails(), 'Do not accept param more than 16 chars')
})
