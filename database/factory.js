'use strict'

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

const { createHash } = require('crypto')
const Factory = use('Factory')
const Config = use('Config')

// tables.users
Factory.blueprint('App/Models/User', async (faker, i, data = {}) => {
  const output = {
    email: data.email || faker.email(),
    username: data.username || faker.username(),
    password: data.password || 'secret'
  }

  output.avatar = [
    'https://s.gravatar.com/avatar',
    createHash('md5').update(output.email).digest('hex')
  ].join('/')

  return output
})

// tables.settings
Factory.blueprint('App/Models/Setting', async (faker, i, data = {}) => ({
  name: faker.sentence({ words: 3 }).trim('.'),
  slug: null,
  description: faker.sentence({ words: 10 }),
  payload: { type: 'string', value: null}
}))

// tables.people
Factory.blueprint('App/Models/Person', async (faker, i, data = {}) => ({
  fullname: faker.name(),
  phone: faker.phone({ formatted: false }),
  address: faker.address(),
  locale: Config.get('app.locales.locale')
}))

// tables.people
Factory.blueprint('App/Models/Role', async (faker, i, data = {}) => ({
  name: faker.sentence({ words: 3 }).trim('.'),
  slug: faker.word({ lenght: 10 }),
  description: faker.sentence({ words: 10 })
}))

// tables.people
Factory.blueprint('App/Models/Permission', async (faker, i, data = {}) => ({
  name: faker.sentence({ words: 3}).trim('.'),
  slug: faker.word({ lenght: 10 }),
  description: faker.sentence({ words: 10 })
}))
