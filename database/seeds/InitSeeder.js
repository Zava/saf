'use strict'

/*
|--------------------------------------------------------------------------
| InstallSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Account = use('App/Services/Account')
const Config = use('Config')
const Database = use('Database')

const project = Config.get('project')
const settings = require('../fixtures/settings')
const tables = require('../fixtures/tables')

class InitSeeder {
  async run () {
    settings.map(setting => {
      setting.payload = JSON.stringify(setting.payload)
      return setting
    })
    await Database.insert(settings).into(tables.settings)

    const user = await use('App/Models/User').create({
      email: 'admin@getsaf.app',
      username: 'admin',
      password: 'secret',
      active_at: new Date()
    })

    const Role = use('App/Models/Role')
    const admin = await Role.create({
      name: 'Administrator',
      slug: 'admin'
    })

    const person = await use('App/Models/Person').create({
      fullname: 'Administrator',
      address: project.address,
    })

    const permissions = Account.scopes

    permissions.map((permission) => {
      permission.scopes = JSON.stringify(permission.scopes)
      return permission
    })

    await admin.permissions().createMany(permissions)

    await user.roles().attach([admin.id])
    await person.user().associate(user)

    const other = await Role.create({
      name: 'Default',
      slug: 'default',
      is_default: true
    })

    const otherPermission = await use('App/Models/Permission').first()
    await other.permissions().attach([otherPermission.id])
  }
}

module.exports = InitSeeder
