'use strict'

const Schema = use('Schema')
const Config = use('Config')

const tables = require('../fixtures/tables')

class InitSchema extends Schema {
  up () {
    this.create(tables.users, (table) => {
      table.increments()
      table.string('username', 80).notNullable().unique()
      table.string('email', 254).notNullable().unique()
      table.string('password', 60).notNullable()
      table.string('avatar').nullable()
      table.timestamp('active_at').nullable()
      table.timestamp('banned_at').nullable()
      table.string('ban_reason').nullable()
      table.timestamps()
    })

    this.create(tables.tokens, (table) => {
      table.increments()
      table.integer('user_id').unsigned()
        .references('id').inTable('users')
        .onDelete('CASCADE').onUpdate('CASCADE')

      table.string('token', 40).notNullable().unique()
      table.string('type', 80).notNullable()
      table.boolean('is_revoked').defaultTo(false)
      table.json('payload').nullable()
      table.timestamps()
    })

    this.create(tables.roles, (table) => {
      table.increments()
      table.string('name', 100).notNullable().unique()
      table.string('slug', 40).notNullable()
      table.text('description').nullable()
      table.boolean('is_default').defaultTo(false)
      table.timestamps()
    })

    this.create(tables.permissions, (table) => {
      table.increments()
      table.string('name', 100).notNullable().unique()
      table.string('slug', 40).notNullable()
      table.text('description').nullable()
      table.json('scopes').default(JSON.stringify([]))
      table.timestamps()
    })

    this.create(tables.users_roles, (table) => {
      table.increments()
      table.integer('user_id').unsigned()
        .references('id').inTable('users')
        .onDelete('CASCADE').onUpdate('CASCADE')

      table.integer('role_id').unsigned()
        .references('id').inTable('roles')
        .onDelete('CASCADE').onUpdate('CASCADE')
    })

    this.create(tables.users_permissions, (table) => {
      table.increments()
      table.integer('user_id').unsigned()
        .references('id').inTable('users')
        .onDelete('CASCADE').onUpdate('CASCADE')

      table.integer('permission_id').unsigned()
        .references('id').inTable('permissions')
        .onDelete('CASCADE').onUpdate('CASCADE')
    })

    this.create(tables.roles_permissions, (table) => {
      table.increments()
      table.integer('role_id').unsigned()
        .references('id').inTable('roles')
        .onDelete('CASCADE').onUpdate('CASCADE')

      table.integer('permission_id').unsigned()
        .references('id').inTable('permissions')
        .onDelete('CASCADE').onUpdate('CASCADE')
    })

    this.create(tables.settings, (table) => {
      table.increments()
      table.integer('user_id').unsigned().nullable()
        .references('id').inTable('users')
        .onDelete('CASCADE').onUpdate('CASCADE')

      table.string('name', 100).notNullable().unique()
      table.string('slug', 40).notNullable()
      table.text('description').nullable()
      table.json('payload').notNullable()
      table.timestamps()
    })

    this.create(tables.people, (table) => {
      table.increments()
      table.integer('user_id').unsigned().nullable()
        .references('id').inTable('users')
        .onDelete('CASCADE').onUpdate('CASCADE')

      table.string('fullname').notNullable().unique()
      table.string('phone', 16).nullable()
      table.text('address').nullable()
      table.string('locale', 2).default(Config.get('app.locales.locale'))
      table.timestamps()
    })
  }

  down () {
    this.drop(tables.people)
    this.drop(tables.settings)
    this.drop(tables.roles_permissions)
    this.drop(tables.users_permissions)
    this.drop(tables.users_roles)
    this.drop(tables.permissions)
    this.drop(tables.roles)
    this.drop(tables.tokens)
    this.drop(tables.users)
  }
}

module.exports = InitSchema
