'use strict'

const Config = use('Config')

module.exports = [
  {
    name: 'company.name',
    slug: 'company_name',
    description: 'company.name.desc',
    payload: {
      type: 'string',
      value: Config.get('project.company')
    },
  },
  {
    name: 'company.address',
    slug: 'company_address',
    description: 'company.address.desc',
    payload: {
      type: 'string',
      value: Config.get('project.address')
    },
  },
  {
    name: 'company.email',
    slug: 'company_email',
    description: 'company.email.desc',
    payload: {
      type: 'string',
      value: Config.get('project.mail.from')
    },
  },
  {
    name: 'site.name',
    slug: 'site_name',
    description: 'site.name.desc',
    payload: {
      type: 'string',
      value: Config.get('project.name')
    },
  },
  {
    name: 'site.description',
    slug: 'site_description',
    description: 'site.description.desc',
    payload: {
      type: 'string',
      value: Config.get('project.description')
    },
  },
  {
    name: 'site.logo',
    slug: 'site_logo',
    description: 'site.logo.desc',
    payload: {
      type: 'file',
      value: ''
    },
  },
  {
    name: 'registration.public',
    slug: 'registration_public',
    description: 'registration.public.desc',
    payload: {
      type: 'boolean',
      value: false
    },
  },
  {
    name: 'registration.captcha',
    slug: 'registration_captcha',
    description: 'registration.captcha.desc',
    payload: {
      dependencies: ['registration.public'],
      type: 'boolean',
      value: false
    },
  },
  {
    name: 'registration.recaptcha',
    slug: 'registration_recaptcha',
    description: 'registration.recaptcha.desc',
    payload: {
      dependencies: ['registration.captcha'],
      type: 'boolean',
      value: false
    },
  },
  {
    name: 'username.blacklist',
    slug: 'username_blacklist',
    description: 'username.blacklist.desc',
    payload: {
      type: 'array',
      value: ['admin','administrator','mod','moderator','root','sys','system']
    },
  },
  {
    name: 'application.rows_limit',
    slug: 'application_rows_limit',
    description: 'application.rows_limit.desc',
    payload: {
      type: 'number',
      value: 10
    },
  },
  {
    name: 'application.date_format',
    slug: 'application_date_format',
    description: 'application.date_format.desc',
    payload: {
      type: 'string',
      value: 'LL'
    },
  },
  {
    name: 'application.datetime_format',
    slug: 'application_datetime_format',
    description: 'application.datetime_format.desc',
    payload: {
      type: 'string',
      value: 'LLL'
    },
  }
]
