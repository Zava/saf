'use strict'

module.exports = {
  users: 'users',
  tokens: 'tokens',
  roles: 'roles',
  permissions: 'permissions',
  users_roles: 'users_roles',
  users_permissions: 'users_permissions',
  roles_permissions: 'roles_permissions',
  settings: 'settings',
  people: 'people'
}
