'use strict'

const { resolve } = require('path')
const { ContextReplacementPlugin } = require('webpack')

const Env = use('Env')
const Helpers = use('Helpers')

// Temporarely Fix : https://github.com/buefy/buefy/issues/712#issuecomment-375470486
global.HTMLElement = typeof window === 'undefined' ? Object : window.HTMLElement

module.exports = {
  mode: 'spa',

  build: {
    publicPath: Env.get('CDN_URL', '/client/'),

    analyze: {
      analyzerMode: 'static'
    },

    postcss: {
      plugins: {
        'postcss-custom-properties': false
      }
    },

    uglify: {
      uglifyOptions: {
        compress: false
      },
      cache: Helpers.tmpPath('uglify')
    },

    plugins: [
      new ContextReplacementPlugin(/moment[\/\\]locale$/, /id/)
    ],

    vendor: ['moment', 'frappe-charts'],

    extractCSS: {
      allChunks: true
    }
  },

  css: [
    '~assets/scss/main.scss'
  ],

  head: {
    meta: [
      {
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width,initial-scale=1'
      },
      {
        'http-equiv': 'x-ua-compatible',
        content: 'ie=edge,chrome=1'
      }
    ],

    link: [
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Montserrat:300,400,600'
      },
      {
        rel: 'stylesheet',
        href: 'https://cdn.materialdesignicons.com/2.0.46/css/materialdesignicons.min.css'
      }
    ]
  },

  modules: [
    'modules/defaults'
  ],

  plugins: [
    '~/plugins/axios'
  ],

  router: {
    base: '/',
    linkActiveClass: 'is-active',
    linkExactActiveClass: 'is-active',

    scrollBehaviour: () => ({ x: 0, y: 0 })
  },

  srcDir: resolve(__dirname, '..', 'resources')
}
