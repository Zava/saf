'use strict'

const Env = use('Env')

module.exports = {
  name: 'SaF Application',

  description: 'Simple as F*ck Application',

  company: 'ACME Company',

  address: 'Somewhere in the planet',

  mail: {
    from: Env.get('MAIL_FROM', 'noreply@acme.com'),

    replyTo: Env.get('MAIL_REPLYTO', 'noreply@acme.com')
  }
}
