'use strict'

const Hash = use('Hash')

const UserHook = module.exports = {}

/**
 * Hash using password as a hook.
 *
 * @param  {User}  userInstance  User model instance
 * @return {void}
 */
UserHook.hashPassword = async (userInstance) => {
  /* istanbul ignore else */
  if (userInstance.password) {
    userInstance.password = await Hash.make(userInstance.password)
  }
}
