'use strict'

const Model = use('Saf/Model')

/**
 * Role Model
 *
 * @class Role
 * @extends {Model}
 */
class Role extends Model {
  /**
   * Default searchable key.
   */
  static get searchKey () {
    return 'name'
  }

  /**
   * @return {BelongsToMany}
   */
  users () {
    return this.belongsToMany('App/Models/User')
      .pivotTable('users_roles')
  }

  /**
   * @return {HasMany}
   */
  permissions () {
    return this.belongsToMany('App/Models/Permission')
      .pivotTable('roles_permissions')
  }
}

module.exports = Role
