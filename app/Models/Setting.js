'use strict'

const Model = use('Saf/Model')

const VALID_TYPES = ['string', 'array', 'object', 'number', 'file']

/**
 * Setting Model
 *
 * @class Setting
 * @extends {Model}
 */
class Setting extends Model {
  static get searchKey () {
    return 'name'
  }

  static get computed () {
    return ['value', 'type', 'dependencies']
  }

  static get Serializer () {
    return 'App/Models/Serializers/Setting'
  }

  /**
   * @return {BelongsTo}
   */
  owner () {
    return this.belongsTo('App/Models/User')
  }

  setPayload (payload) {
    if (!payload.type) {
      return {
        type: 'string',
        value: payload
      }
    }

    if (VALID_TYPES.indexOf(payload.type) < 0) {
      payload.type = 'string'
    }

    return JSON.stringify(payload)
  }

  getPayload (payload) {
    return typeof payload === 'string' ? JSON.parse(payload) : payload
  }

  getDependencies ({ payload }) {
    return Array.isArray(payload.dependencies)
      ? payload.dependencies.map(dep => dep.replace('.', '_'))
      : []
  }

  getType ({ payload }) {
    return payload.type
  }

  getValue ({ payload }) {
    const { type, value } = payload

    if (type === 'number') return Number(value)

    return value
  }
}

module.exports = Setting
