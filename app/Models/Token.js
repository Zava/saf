'use strict'

const Model = use('Saf/Model')

/**
 * Token Model
 *
 * @class Token
 * @extends {Model}
 */
class Token extends Model {
}

module.exports = Token
