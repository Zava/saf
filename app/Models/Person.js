'use strict'

const Model = use('Saf/Model')

/**
 * Person Model
 *
 * @class Person
 * @extends {Model}
 */
class Person extends Model {
  /**
   * Default searchable key.
   */
  static get searchKey () {
    return 'fullname'
  }

  /**
   * @return {BelongsTo}
   */
  user () {
    return this.belongsTo('App/Models/User')
  }
}

module.exports = Person
