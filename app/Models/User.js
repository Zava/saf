'use strict'

const Model = use('Saf/Model')

/**
 * User Model
 *
 * @class User
 * @extends {Model}
 * @property {boolean} isActive Indicate this account is activated or not.
 * @property {boolean} isBanned Indicate this account is banned or not.
 */
class User extends Model {
  static boot () {
    super.boot()

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook('beforeCreate', 'UserHook.hashPassword')
  }

  static get hidden () {
    return ['password']
  }

  static get searchKey () {
    return 'username'
  }

  static get computed () {
    return ['is_active', 'is_banned']
  }

  static get dates () {
    return super.dates.concat(['active_at', 'banned_at'])
  }

  /**
   * A relationship on tokens is required for auth to work.
   * Since features like `refreshTokens` or `rememberToken`
   * will be saved inside the tokens table.
   *
   * @return {HasMany}
   */
  tokens () {
    return this.hasMany('App/Models/Token')
  }

  /**
   * Role relationship.
   *
   * @return {HasMany}
   */
  roles () {
    return this.belongsToMany('App/Models/Role')
      .pivotTable('users_roles')
  }

  /**
   * Permissions relationship.
   *
   * @return {HasMany}
   */
  permissions () {
    return this.belongsToMany('App/Models/Permission')
      .pivotTable('users_permissions')
  }

  /**
   * Profile relationship.
   *
   * @return {HasOne}
   */
  profile () {
    return this.hasOne('App/Models/Person')
  }

  /**
   * @private
   * @param  {object}  param0  User attributes
   * @return {boolean}
   */
  /* eslint-disable camelcase */
  getIsActive ({ active_at }) {
    return !!active_at
  }

  /**
   * @private
   * @param  {object}  param0  User attributes
   * @return {boolean}
   */
  /* eslint-disable camelcase */
  getIsBanned ({ banned_at }) {
    return !!banned_at
  }
}

module.exports = User
