'use strict'

const { localize } = use('Saf/Helpers')

class SettingSerializer {
  constructor (fields, pages = null, isOne = false) {
    this.fields = fields
    this.isOne = isOne
  }

  get rows () {
    return this.fields
  }

  get slugs () {
    return this.fields.map(field => field.slug)
  }

  get rules () {
    const rules = {}

    for (let { slug, payload } of this.fields) {
      const rule = [payload.type]

      if (payload.value) {
        rule.push('required')
      }

      rules[slug] = rule.join('|')
    }

    return rules
  }

  get (name, defaultValue) {
    const setting = this.fields.find((field) => {
      return field.slug === name.replace(/\./g, '_')
    })

    return setting ? setting.payload.value : defaultValue
  }

  _getRowJSON (row) {
    const setting = row.toObject()

    setting.slug = setting.slug
    setting.name = localize('settings', setting.name) || setting.name
    setting.description = localize('settings', setting.description) || setting.description

    return setting
  }

  entries () {
    const settings = {}

    for (let { name, slug, description, payload } of this.fields) {
      let { type, value, dependencies } = payload

      if (Array.isArray(dependencies)) {
        dependencies = dependencies.map(dep => dep.replace(/\./g, '_'))
      }

      settings[slug] = {
        name: localize('settings', name) || name,
        description: localize('settings', description) || description,
        dependencies,
        value,
        type
      }
    }

    return settings
  }

  toJSON () {
    if (this.isOne) {
      return this._getRowJSON(this.fields)
    }

    return this.fields.map(this._getRowJSON.bind(this))
  }
}

module.exports = SettingSerializer
