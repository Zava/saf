'use strict'

const Model = use('Saf/Model')

/**
 * Permission Model.
 *
 * @class Permission
 * @extends {Model}
 */
class Permission extends Model {
  /**
   * Default searchable key.
   */
  static get searchKey () {
    return 'name'
  }

  /**
   * @return {BelongsToMany}
   */
  users () {
    return this.belongsToMany('App/Models/User')
      .pivotTable('users_permissions')
  }

  /**
   * @return {BelongsToMany}
   */
  roles () {
    return this.belongsToMany('App/Models/Role')
      .pivotTable('roles_permissions')
  }
}

module.exports = Permission
