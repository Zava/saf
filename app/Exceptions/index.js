'use strict'

const {
  LogicalException,
  RuntimeException
} = require('@adonisjs/generic-exceptions')

class AccountException extends LogicalException {
  static get repo () {
    return 'feryardiant/saf'
  }
}

class InvalidRequestTokenException extends AccountException {
  static invoke () {
    return new this('Invalid request token', 401, 'E_INVALID_REQUEST_TOKEN')
  }
}

class UnauthorizedAccessException extends AccountException {
  static invoke () {
    return new this('Unauthorized Access', 401, 'E_UNAUTHORIZED_ACCESS')
  }
}

class ServiceUnavailable extends RuntimeException {
  static invoke () {
    return new this('Service Unavailable', 503)
  }
}

module.exports = {
  InvalidRequestTokenException,
  UnauthorizedAccessException,
  ServiceUnavailable
}
