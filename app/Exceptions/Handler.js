'use strict'

const BaseExceptionHandler = use('BaseExceptionHandler')

/**
 * This class handles all exceptions thrown during
 * the HTTP request lifecycle.
 *
 * @class ExceptionHandler
 */
class ExceptionHandler extends BaseExceptionHandler {
  /**
   * Do not reports certain exceptions.
   */
  get doNotReports () {
    return [
      'HttpException',
      'InvalidBasicAuthException',
      'InvalidSessionException',
      'InvalidApiToken',
      'InvalidJwtToken',
      'ExpiredJwtToken',
      'UnauthorizedAccessException',
      'InvalidRefreshToken',
      'InvalidRequestTokenException',
      'ModelNotFoundException',
      'PasswordMisMatchException',
      'UserNotFoundException',
      'ValidationException',
      'ServiceUnavailable'
    ]
  }

  /**
   * Formatting error for non dev environment.
   *
   * @param  {Error}   error  Error instance
   * @param  {object}  antl   Translation
   * @return {object}
   */
  _getFormattedError (error, antl) {
    const output = {
      name: error.name,
      code: error.code
    }

    try {
      output.message = antl.formatMessage(`exceptions.${error.name}`)
    } catch (e) {
      output.message = error.message
    }

    if (error.name === 'ValidationException' && error.messages.length) {
      output.fields = {}
      for (const msg of error.messages) {
        if (!output.fields[msg.field]) {
          output.fields[msg.field] = []
        }

        output.fields[msg.field].push({
          message: msg.message,
          validation: msg.validation
        })
      }
    }

    return output
  }

  /**
   * Handle error thrown.
   *
   * @param  {object}  error         Error thrown
   * @param  {object}  ctx           Adonis context
   * @param  {object}  ctx.request   Adonis Request instance
   * @param  {object}  ctx.response  Adonis Response instance
   * @param  {object}  ctx.view      Adonis View
   * @param  {object}  ctx.antl      Adonis Internalization instance
   * @return {void}
   */
  async handle (error, { request, response, view, antl }) {
    const isJSON = request.accepts(['html', 'json']) === 'json'

    response.status(error.status)

    if (process.env.NODE_ENV === 'production' && !isJSON) {
      return response.send(
        view.render(`errors.${error.status}`)
      )
    }

    if (process.env.NODE_ENV === 'development' && this.shouldReports(error)) {
      const formattedError = await this._getYouchError(error, request.request, isJSON)
      return response.send(formattedError.error)
    }

    if (isJSON) {
      const formattedError = this._getFormattedError(error, antl)
      return response.send(formattedError)
    }

    return super.handle(...arguments)
  }

  /**
   * Report exception for logging or debugging.
   *
   * @param  {object}  error        Error thrown
   * @param  {object}  ctx          Adonis context
   * @param  {object}  ctx.request  Adonis Request class
   * @param  {object}  ctx.auth     Adonis Authentication class
   * @return {void}
   */
  async report (error, { request, auth }) {
    if (process.env.NODE_ENV !== 'testing' && this.shouldReports(error)) {
      try {
        await use('Sentry/Client').capture(error, request, auth)
      } catch (e) {
        use('Adonis/Src/Logger').error(error.message, { error })
      }
    }

    return super.report(...arguments)
  }

  /**
   * Determine should the app report the error?
   *
   * @param  {Error}  error  Error thrown
   * @return {boolean}
   */
  shouldReports (error) {
    return this.doNotReports.indexOf(error.name) === -1
  }
}

module.exports = ExceptionHandler
