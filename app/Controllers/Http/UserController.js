'use strict'

const Account = use('App/Services/Account')
const Controller = use('Saf/Controller')
const Model = use('App/Models/User')

class UserController extends Controller {
  /**
   * Form fields
   */
  static get fields () {
    return ['username', 'email', 'password', 'ban', 'reason', 'activate']
  }

  static get sanitizationRules () {
    return {
      username: 'escape',
      email: 'escape',
      activate: 'to_boolean',
      ban: 'to_boolean',
      reason: 'escape'
    }
  }

  /**
   * Show all users.
   *
   * @async
   * @param  {HttpContext}  ctx           Adonis Http Context
   * @param  {Request}      ctx.request   Adonis Http Request
   * @return {Promise<any>}
   */
  index ({ request }) {
    return Model.filterBy(request.filter())
  }

  /**
   * Create new user.
   *
   * @async
   * @param  {HttpContext}  ctx           Adonis Http Context
   * @param  {Request}      ctx.request   Adonis Http Request
   * @param  {Response}     ctx.response  Adonis Http Response
   * @param  {Auth}         ctx.auth      Adonis Auth
   * @param  {Antl}         ctx.antl      Adonis Translation
   * @return {Promise<any>}
   */
  async store ({ request, response, auth, antl }) {
    const { email } = await this.validate(request, {
      email: 'required|unique:users,email'
    })

    const model = await Account.inviteUser(auth, email)

    return response.status(201).send({
      message: antl.formatMessage('users.model.created'),
      model
    })
  }

  /**
   * Show user by id.
   *
   * @async
   * @param  {HttpContext}  ctx          Adonis Http Context
   * @param  {Object}       ctx.params   Adonis Route params
   * @return {Promise<any>}
   */
  async show ({ params }) {
    const model = await Model.findOrFail(params.id)

    return { model }
  }

  /**
   * Update user by id.
   *
   * @async
   * @param  {HttpContext}  ctx          Adonis Http Context
   * @param  {Request}      ctx.request  Adonis Http Request
   * @param  {Object}       ctx.params   Adonis Route params
   * @param  {Auth}         ctx.auth     Adonis Auth
   * @param  {Antl}         ctx.antl     Adonis Translation
   * @return {Promise<any>}
   */
  async update ({ request, params, auth, antl }) {
    const { ban, reason, activate } = await this.validate(request, {
      ban: 'boolean',
      reason: 'string',
      activate: 'boolean'
    })

    const model = await Model.findOrFail(params.id)

    if ((activate || ban) && ban !== activate) {
      await Account.banOrActivateUser(auth, model, { ban, reason, activate })
    }

    return {
      message: antl.formatMessage('users.model.updated'),
      model
    }
  }

  /**
   * Delete user by id.
   *
   * @async
   * @param  {HttpContext}  ctx         Adonis Http Context
   * @param  {Object}       ctx.params  Adonis Route params
   * @param  {Antl}         ctx.antl    Adonis Translation
   * @return {Promise<any>}
   */
  async destroy ({ params, antl }) {
    const data = await Model.findOrFail(params.id)

    await data.delete()

    return {
      message: antl.formatMessage('users.model.deleted')
    }
  }
}

module.exports = UserController
