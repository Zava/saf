'use strict'

const Model = use('App/Models/Setting')
const Controller = use('Saf/Controller')
// const Database = use('Database')

class SettingController extends Controller {
  /**
   * Show all users.
   *
   * @async
   * @param  {HttpContext}        ctx           Adonis Http Context
   * @return {Promise<any>}
   */
  async index () {
    const settings = await Model.all()

    return settings.entries()
  }

  /**
   * Update user by id.
   *
   * @async
   * @param  {HttpContext}        ctx           Adonis Http Context
   * @param  {Request}            ctx.request   Adonis Http Request
   * @param  {Object}             ctx.params    Adonis Route params
   * @param  {Antl}               ctx.antl      Adonis Translation
   * @return {Promise<any>}
   */
  async update ({ request, params, antl }) {
    const settings = await Model.all()

    this.constructor.fields = settings.slugs

    const data = await this.validate(request, settings.rules)
    const entries = settings.entries()

    Promise.all(Object.entries(data).map(([field, value]) => {
      const setting = entries[field]

      if (settings.get(field) !== value && setting) {
        return Model.query().where('slug', field).update({
          payload: {
            type: setting.type,
            dependencies: setting.dependencies || [],
            value
          }
        })
      }

      return false
    }))

    const updated = await Model.all()

    return {
      message: antl.formatMessage('settings.data.updated'),
      data: updated.entries()
    }
  }
}

module.exports = SettingController
