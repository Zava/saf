'use strict'

const Model = use('App/Models/Person')
const Controller = use('Saf/Controller')

class PersonController extends Controller {
  /**
   * Form fields
   */
  static get fields () {
    return ['fullname', 'address', 'phone', 'locale']
  }

  /**
   * Show all roles.
   *
   * @async
   * @param  {HttpContext}  ctx           Adonis Http Context
   * @param  {Request}      ctx.request   Adonis Http Request
   * @return {Promise<any>}
   */
  index ({ request }) {
    return Model.filterBy(request.filter())
  }

  /**
   * Create new role.
   *
   * @async
   * @param  {HttpContext}  ctx           Adonis Http Context
   * @param  {Request}      ctx.request   Adonis Http Request
   * @param  {Response}     ctx.response  Adonis Http Response
   * @param  {Antl}         ctx.antl      Adonis Translation
   * @return {Promise<any>}
   */
  async store ({ request, response, antl }) {
    const data = await this.validate(request, {
      fullname: 'required',
      address: 'string',
      phone: 'phone'
    })

    const model = await Model.create(data)

    return response.status(201).send({
      message: antl.formatMessage('people.model.created'),
      model
    })
  }

  /**
   * Show role by id.
   *
   * @async
   * @param  {HttpContext}  ctx          Adonis Http Context
   * @param  {Object}       ctx.params   Adonis Route params
   * @return {Promise<any>}
   */
  async show ({ params }) {
    const model = await Model.findOrFail(params.id)

    return { model }
  }

  /**
   * Update role by id.
   *
   * @async
   * @param  {HttpContext}  ctx          Adonis Http Context
   * @param  {Request}      ctx.request  Adonis Http Request
   * @param  {Object}       ctx.params   Adonis Route params
   * @param  {Antl}         ctx.antl     Adonis Translation
   * @return {Promise<any>}
   */
  async update ({ request, params, antl }) {
    const data = await this.validate(request, {
      fullname: 'required',
      address: 'string',
      phone: 'phone'
    })

    const model = await Model.findOrFail(params.id)

    model.merge(data)

    await model.save()

    return {
      message: antl.formatMessage('people.model.updated'),
      model
    }
  }

  /**
   * Delete role by id.
   *
   * @async
   * @param  {HttpContext}  ctx         Adonis Http Context
   * @param  {Object}       ctx.params  Adonis Route params
   * @param  {Antl}         ctx.antl    Adonis Translation
   * @return {Promise<any>}
   */
  async destroy ({ params, antl }) {
    const data = await Model.findOrFail(params.id)

    await data.delete()

    return {
      message: antl.formatMessage('people.model.deleted')
    }
  }
}

module.exports = PersonController
