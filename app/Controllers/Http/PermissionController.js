'use strict'

const Model = use('App/Models/Permission')
const Controller = use('Saf/Controller')

class PermissionController extends Controller {
  /**
   * Form fields
   */
  static get fields () {
    return ['name', 'slug', 'description']
  }

  /**
   * Show all permissions.
   *
   * @async
   * @param  {HttpContext}  ctx           Adonis Http Context
   * @param  {Request}      ctx.request   Adonis Http Request
   * @return {Promise<any>}
   */
  index ({ request }) {
    return Model.filterBy(request.filter())
  }

  /**
   * Create new permission.
   *
   * @async
   * @param  {HttpContext}  ctx           Adonis Http Context
   * @param  {Request}      ctx.request   Adonis Http Request
   * @param  {Response}     ctx.response  Adonis Http Response
   * @param  {Antl}         ctx.antl      Adonis Translation
   * @return {Promise<any>}
   */
  async store ({ request, response, antl }) {
    const data = await this.validate(request, {
      name: 'required',
      slug: 'required|unique:permissions,slug',
      description: 'string'
    })

    const model = await Model.create(data)

    return response.status(201).send({
      message: antl.formatMessage('permissions.model.created'),
      model
    })
  }

  /**
   * Show permission by id.
   *
   * @async
   * @param  {HttpContext}  ctx         Adonis Http Context
   * @param  {Object}       ctx.params  Adonis Route params
   * @return {Promise<any>}
   */
  async show ({ params }) {
    const model = await Model.findOrFail(params.id)

    return { model }
  }

  /**
   * Update permission by id.
   *
   * @async
   * @param  {HttpContext}  ctx          Adonis Http Context
   * @param  {Request}      ctx.request  Adonis Http Request
   * @param  {Object}       ctx.params   Adonis Route params
   * @param  {Antl}         ctx.antl     Adonis Translation
   * @return {Promise<any>}
   */
  async update ({ request, params, antl }) {
    const data = await this.validate(request, {
      name: 'required',
      slug: `required|unique:permissions,slug,${params.id}`,
      description: 'string'
    })

    const model = await Model.findOrFail(params.id)

    model.merge(data)

    await model.save()

    return {
      message: antl.formatMessage('permissions.model.updated'),
      model
    }
  }

  /**
   * Delete permission by id.
   *
   * @async
   * @param  {HttpContext}  ctx         Adonis Http Context
   * @param  {Object}       ctx.params  Adonis Route params
   * @param  {Antl}         ctx.antl    Adonis Translation
   * @return {Promise<any>}
   */
  async destroy ({ params, antl }) {
    const data = await Model.findOrFail(params.id)

    await data.delete()

    return {
      message: antl.formatMessage('permissions.model.deleted')
    }
  }
}

module.exports = PermissionController
