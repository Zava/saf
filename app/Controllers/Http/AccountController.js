'use strict'

const Account = use('App/Services/Account')
const Controller = use('Saf/Controller')
const Model = use('App/Models/User')

class AccountController extends Controller {
  /**
   * Form fields
   */
  static get fields () {
    return ['username', 'email', 'password', 'password_confirmation']
  }

  /**
   * Field validation rules
   */
  static get validationRules () {
    return {
      username: 'required|unique:users,username',
      email: 'required|unique:users,email',
      password: 'required|confirmed'
    }
  }

  /**
   * Login.
   *
   * @api {post} /account/login
   *
   * @param  {HttpContext}  ctx          Adonis Http Context
   * @param  {Request}      ctx.request  Adonis Http Request
   * @param  {Object}       ctx.params   Adonis Route params
   * @param  {Auth}         ctx.auth     Adonis Auth
   * @param  {Antl}         ctx.antl     Adonis Translation
   * @return {Promise<any>}
   */
  async login ({ request, response, auth, antl }) {
    const input = request.only(['username', 'password'])

    const { username, password } = await this.validate(input, {
      username: 'required',
      password: 'required'
    })

    const { token } = await auth.attempt(username, password)

    const user = await Model.findBy('username', username)

    return response.header('X-API-TOKEN', token).send({
      message: antl.formatMessage('account.logged-in'),
      api_token: token,
      account: await Account.makeResponse(user)
    })
  }

  /**
   * Register new user.
   *
   * @api {post} /account/register
   *
   * @param  {HttpContext}  ctx           Adonis Http Context
   * @param  {Request}      ctx.request   Adonis Http Request
   * @param  {Response}     ctx.response  Adonis Http Response
   * @param  {Antl}         ctx.antl     Adonis Translation
   * @return {Promise<any>}
   */
  async register ({ request, response, antl }) {
    const { username, email, password } = await this.validate(request)

    const user = await Model.create({ username, email, password })

    await Account.requestActivation(user)

    return response.status(201).send({
      message: antl.formatMessage('account.request-activation')
    })
  }

  /**
   * Logout.
   *
   * @api {post} /account/logout
   *
   * @param  {HttpContext}  ctx           Adonis Http Context
   * @param  {Response}     ctx.response  Adonis Http Response
   * @param  {Auth}         ctx.auth      Adonis Auth
   * @param  {Antl}         ctx.antl      Adonis Translation
   * @return {Promise<any>}
   */
  async logout ({ response, auth, antl }) {
    const apiToken = auth.getAuthHeader()

    await auth.revokeTokens([apiToken])

    return response.status(200).send({
      message: antl.formatMessage('account.logged-out')
    })
  }

  /**
   * Forgot password.
   *
   * @api {post} /account/forgot
   *
   * @param  {HttpContext}  ctx          Adonis Http Context
   * @param  {Request}      ctx.request  Adonis Http Request
   * @param  {Antl}         ctx.antl     Adonis Translation
   * @return {Object}
   */
  async forgot ({ request, antl }) {
    const input = request.only(['credential'])

    const { credential } = await this.validate(input, {
      credential: 'required'
    })

    await Account.requestPassword(credential)

    return {
      message: antl.formatMessage('account.request-update-password')
    }
  }

  /**
   * Reset current password or email.
   *
   * @api {get} /account/confirm/:type
   *
   * @param  {HttpContext}  ctx           Adonis Http Context
   * @param  {Request}      ctx.request   Adonis Http Request
   * @param  {Response}     ctx.response  Adonis Http Response
   * @param  {Object}       ctx.params    Adonis Route params
   * @param  {Antl}         ctx.antl      Adonis Translation
   * @return {Object}
   */
  async confirm ({ request, response, params, antl }) {
    const token = request.input('token')
    const user = await Account.applyRequest(params.type, token)

    if (params.type === 'activation') {
      response.status(201)
    }

    return {
      message: antl.formatMessage(`account.confirm-${params.type}`, {
        username: user.username
      })
    }
  }

  /**
   * Show current user account.
   *
   * @api {get} /account
   *
   * @param  {HttpContext}  ctx       Adonis Http Context
   * @param  {Auth}         ctx.auth  Adonis Auth
   * @return {Object}
   */
  async show ({ auth }) {
    const account = await Account.makeResponse(auth.user)

    return { account }
  }

  /**
   * Update current user account.
   *
   * @api {put} /account
   *
   * @param  {HttpContext}  ctx          Adonis Http Context
   * @param  {Request}      ctx.request  Adonis Http Request
   * @param  {Auth}         ctx.auth     Adonis Auth
   * @param  {Antl}         ctx.antl     Adonis Translation
   * @return {Object}
   */
  async update ({ request, auth, antl }) {
    const data = await this.validate(request, {
      username: `required|unique:users,username,id,${auth.user.id}`,
      email: `required|unique:users,email,id,${auth.user.id}`,
      password: 'confirmed'
    })

    await Account.updateCredential(auth.user, data)

    const user = await auth.getUser()
    const account = await Account.makeResponse(auth.user)

    return {
      message: antl.formatMessage(
        data.email === user.email ? 'account.updated' : 'account.updated-emailed'
      ),
      account
    }
  }
}

module.exports = AccountController
