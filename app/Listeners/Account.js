'use strict'

const Kue = use('Kue')

const Account = exports = module.exports = {}

Account.requestActivation = ({ user, token }) => {
  return Kue.dispatch('mailer', {
    type: 'activation',
    recipient: user,
    token
  })
}

Account.requestPassword = ({ user, token }) => {
  return Kue.dispatch('mailer', {
    type: 'update-password',
    recipient: user,
    token
  })
}

Account.requestEmail = ({ user, token, email }) => {
  return Kue.dispatch('mailer', {
    type: 'update-email',
    recipient: user,
    token,
    email
  })
}

Account.proceedInvitation = ({ user, host, token, password }) => {
  return Kue.dispatch('mailer', {
    type: 'invitation',
    recipient: user,
    host,
    token,
    password
  })
}

Account.confirmed = ({ user, type }) => {
  type = (type !== 'activation')
    ? type.split(/(?=[A-Z])/).join('-').toLowerCase()
    : 'welcome'

  return Kue.dispatch('mailer', { type, recipient: user })
}
