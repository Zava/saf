'use strict'

const { Command } = require('@adonisjs/ace')

class Nuxt extends Command {
  /**
   * signature defines the requirements and name
   * of command.
   *
   * @return {String}
   */
  static get signature () {
    return 'build:nuxt'
  }

  /**
   * description is the little helpful information displayed
   * on the console.
   *
   * @return {String}
   */
  static get description () {
    return 'Build for production the nuxt.js application.'
  }

  /**
   * handle method is invoked automatically by ace, once your
   * command has been executed.
   */
  async handle () {
    await use('App/Services/Nuxt').build()
  }
}

module.exports = Nuxt
