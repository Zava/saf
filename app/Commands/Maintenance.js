'use strict'

const { Command } = require('@adonisjs/ace')
const Helpers = use('Helpers')

class Maintenance extends Command {
  static get signature () {
    return `
      maintenance
      { enable : Enable/Disable Maintenance mode }
    `
  }

  static get description () {
    return 'Maintenance mode'
  }

  static get filePath () {
    return Helpers.tmpPath('.maintenance')
  }

  async handle (args, options) {
    const enable = this._parseArgs(args)
    let message = ''

    if (enable) {
      message = 'Maintenance mode enabled'
      await this.ensureFile(this.constructor.filePath)
    } else {
      message = 'Maintenance mode disabled'
      await this.removeFile(this.constructor.filePath)
    }

    this.info(`${this.icon('success')} ${message}`)
    process.exit(0)
  }

  _parseArgs ({ enable }) {
    const obj = {
      yes: true,
      no: false,
      on: true,
      off: false
    }

    return obj[enable] || null
  }
}

module.exports = Maintenance
