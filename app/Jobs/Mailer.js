'use strict'

const Mail = use('Mail')
const Helpers = use('Saf/Helpers')
const Logger = use('Logger')
const project = use('Config').get('project')

class Mailer {
  static get concurrency () {
    return 2
  }

  static get key () {
    return 'mailer'
  }

  async handle ({ type, recipient, ...data }) {
    Logger.info('Mailer-job started')

    data.user = recipient
    data.type = type
    if (data.token) {
      data.token = encodeURIComponent(data.token)
    }

    const view = `emails.${type}`
    const subject = Helpers.localize('emails', type, {
      project: project.name
    })

    Logger.info('Mailer-job sending', data)
    await Mail.send(view, data, (mail) => {
      mail.to(recipient.email, recipient.username)
        .from(project.mail.from, project.name)
        .subject(subject || 'Email Subject')
    })
  }
}

module.exports = Mailer

