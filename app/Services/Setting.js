'use strict'

class Setting {
  constructor (Model) {
    this.Model = Model
    this.items = null
  }

  async boot () {
    if (!this.items) {
      this.items = await this.Model.all()
    }

    return this
  }

  get (name, defaultValue) {
    return this.items ? this.items.get(name, defaultValue) : defaultValue
  }

  entries () {
    return this.items ? this.items.entries() : {}
  }
}

module.exports = Setting
