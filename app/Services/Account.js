'use strict'

const { generate } = require('randomstring')
const { UserNotFoundException } = require('@adonisjs/auth/src/Exceptions')

const Event = use('Event')
const Route = use('Route')
const Encryption = use('Adonis/Src/Encryption')
const User = use('App/Models/User')
const Role = use('App/Models/Role')
const {
  InvalidRequestTokenException,
  UnauthorizedAccessException
} = use('App/Exceptions')

class AccountService {
  /**
   * Ger authentication scopes.
   *
   * @return {string[]}
   */
  get scopes () {
    const scopes = []

    for (const route of Route.list()) {
      if (!route.middlewareList.includes('auth')) {
        continue
      }

      const paths = route.name.split('.').slice(0, -1)
      const resource = {
        name: paths.join('.'),
        slug: paths.concat('manage').join('.'),
        scopes: [route.name]
      }
      const group = scopes.find(r => r.name === resource.name)

      if (group) {
        group.scopes.push(route.name)
      } else {
        scopes.push(resource)
      }
    }

    return scopes
  }

  /**
   * Authorize the user with the current route.
   *
   * @param  {Request}  request  Adonis Http Request
   * @param  {Auth}     auth     App User model instance
   * @throws {UnauthorizedAccessException}
   */
  async authorize (request, auth) {
    const account = auth.user

    await account.load('roles.permissions')

    const { roles } = account.toJSON()

    if (roles.length === 0) {
      throw UnauthorizedAccessException.invoke()
    }

    if (roles.find(role => role.slug === 'admin') !== undefined) return

    const scopes = this.getUserScopes(roles)

    if (!scopes.includes(request.route().name)) {
      throw UnauthorizedAccessException.invoke()
    }
  }

  /**
   * Get default role.
   *
   * @return {Promise<Role>}
   */
  getDefaultRole () {
    return Role.query().where('is_default', true).first()
  }

  /**
   * Create new token for user.
   *
   * @memberof AccountService
   * @private
   * @param  {User}    user       User instance
   * @param  {string}  type       Token type
   * @param  {object}  [payload]  Extra payload
   * @return {string}
   */
  async createUserToken (user, type, payload = {}) {
    const token = generate(10)

    await user.tokens().create({ type, token, payload, is_revoked: false })

    return Encryption.encrypt(token)
  }

  /**
   * Find User by encrypted token string.
   *
   * @memberof AccountService
   * @private
   * @param  {string}  type       Token type
   * @param  {any}     encrypted  Encrypted token string
   * @return {Promise<User>}
   */
  findUserByToken (type, encrypted) {
    const token = Encryption.decrypt(encrypted)

    return User.query().whereHas('tokens', (query) => {
      query.where({ type, token, is_revoked: false })
    }).with('tokens').first()
  }

  /**
   * Revoke existing token.
   *
   * @memberof AccountService
   * @private
   * @param  {User}    user       User instance
   * @param  {string}  type       Token type
   * @param  {any}     encrypted  Encrypted token string
   * @return {boolean}
   */
  revokeUserToken (user, type, encrypted) {
    const token = Encryption.decrypt(encrypted)

    return user.tokens().where({ type, token, is_revoked: false }).update({
      is_revoked: true
    })
  }

  /**
   * Find user by email or username.
   *
   * @param  {string}  credential  User credential
   * @return {Promise<User>}
   */
  findUserByCredential (credential) {
    return User.query().where((qb) => {
      qb.where('username', credential).orWhere('email', credential)
    }).first()
  }

  /**
   * Apply user's email changes.
   *
   * @memberof AccountService
   * @param  {string}  type   Request type
   * @param  {string}  token  Encrypted Confirmation token
   * @return {Promise<User>}
   */
  async applyRequest (type, token) {
    const user = await this.findUserByToken(type, token)

    if (user === null) {
      throw InvalidRequestTokenException.invoke()
    }

    if (type === 'activation') {
      user.merge({
        active_at: new Date()
      })
    } else {
      const { payload } = user.getRelated('tokens').first()

      user.merge(payload)
    }

    await user.save()

    if (type === 'activation') {
      const role = await this.getDefaultRole()

      await user.roles().attach([role.id])

      await user.profile().create({
        fullname: user.username
      })
    }

    await this.revokeUserToken(user, type, token)

    Event.emit('account:confirmed', { user, type })

    return user
  }

  /**
   * Sent activation email to new user.
   *
   * @memberof AccountService
   * @param  {User}  user  User instance
   */
  async requestActivation (user) {
    const token = await this.createUserToken(user, 'activation')

    Event.emit('account:activation', { user, token })
  }

  /**
   * Request new user email.
   *
   * @memberof AccountService
   * @param  {User}    user  User instance
   * @param  {object}  data  New user data
   */
  async updateCredential (user, data) {
    user.username = data.username

    if (data.password) {
      const changed = await use('Hash').verify(data.password, user.password)

      if (changed) {
        user.password = data.password
      }
    }

    if (user.email !== data.email) {
      const token = await this.createUserToken(user, 'update-email', { email: data.email })

      Event.emit('account:updateCredential', { user, token, email: data.email })
    }

    await user.save()
  }

  /**
   * Request new user password.
   *
   * @memberof AccountService
   * @param  {User}  user  User instance
   */
  async requestPassword (user) {
    if (typeof user === 'string') {
      user = await this.findUserByCredential(user)
    }

    if (user === null) {
      throw UserNotFoundException.invoke('Cannot find user', 'credential', 'password', 'api')
    }

    const token = await this.createUserToken(user, 'update-password')

    Event.emit('account:updatePassword', { user, token })
  }

  /**
   * Invite more user.
   *
   * @memberof AccountService
   * @param  {object}  auth   Authentication instance
   * @param  {string}  email  Guess email address
   * @return {Promise<User>}
   */
  async inviteUser (auth, email) {
    const username = email.split('@').shift()
    const password = generate(10)

    const user = await User.create({ username, email, password })
    const token = await this.createUserToken(user, 'invitation')

    Event.emit('account:invitation', { host: auth.user, user, token, password })

    return user
  }

  async banOrActivateUser (auth, user, { activate, ban, reason }) {
    const data = {
      active_at: null,
      banned_at: null,
      ban_reason: null
    }

    if (ban === undefined && activate === true) {
      data.active_at = new Date()
    }

    if (activate === undefined && ban === true) {
      data.banned_at = new Date()
      data.ban_reason = reason || null
    }

    user.merge(data)

    await user.save()

    return user
  }

  async makeResponse (user) {
    const account = user.toJSON()

    if (!user.getRelated('roles')) {
      await user.load('roles.permissions')
    }

    if (Array.isArray(account.roles)) {
      delete account.roles
    }

    await user.loadMany(['profile', 'permissions'])
    const profile = user.getRelated('profile')

    account.fullname = profile.fullname
    account.phone = profile.phone
    account.address = profile.address
    account.scopes = this.getUserScopes(user.getRelated('roles').toJSON())

    return account
  }

  getUserScopes (roles) {
    const scopes = []

    for (const role of roles) {
      for (const permission of role.permissions) {
        scopes.push(...(permission.scopes || []))
      }
    }

    return scopes
  }
}

module.exports = new AccountService()
