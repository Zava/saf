'use strict'

const Config = use('Config')
const Env = use('Env')
const Helpers = use('Helpers')
const { Nuxt, Builder } = require('nuxt')

class NuxtService {
  constructor () {
    this.nuxt = null
  }

  get isDev () {
    return Env.get('NODE_ENV') === 'development'
  }

  get config () {
    const locale = Config.get('app.locales.locale')
    const project = Config.get('project')

    return Config.merge('nuxt', {
      dev: this.isDev,
      env: { project, locale, color: '#589d8d' },
      head: {
        title: project.name
      }
    })
  }

  build () {
    this.nuxt = new Nuxt(this.config)

    if (Helpers.isAceCommand() || this.isDev) {
      return new Builder(this.nuxt).build()
    }
  }

  async render (req, res) {
    await this.nuxt.render(req.request, res.response)
  }
}

module.exports = new NuxtService()
