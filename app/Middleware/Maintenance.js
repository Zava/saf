'use strict'

const Drive = use('Drive')
const { ServiceUnavailable } = use('App/Exceptions')

class Maintenance {
  async handle ({ view }, next) {
    const down = await Drive.disk('local').exists('.maintenance')

    if (down) {
      // return view.render('errors.503')
      throw ServiceUnavailable.invoke()
    }

    await next()
  }
}

module.exports = Maintenance
