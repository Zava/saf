'use strict'

const Account = use('App/Services/Account')

class Resource {
  async handle ({ request, auth, response }, next) {
    // response.implicitEnd = false

    await Account.authorize(request, auth)

    await next()
  }
}

module.exports = Resource
