'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

const Route = use('Route')

Route.group('account', () => {
  Route.get('', 'AccountController.show').as('account.show').middleware(['auth', 'api'])
  Route.put('', 'AccountController.update').as('account.update').middleware(['auth', 'api'])

  Route.post('login', 'AccountController.login').as('account.login')
  Route.post('register', 'AccountController.register').as('account.register')
  Route.post('logout', 'AccountController.logout').as('account.logout').middleware(['auth', 'api'])

  Route.post('forgot', 'AccountController.forgot').as('account.forgot')
  Route.get('confirm/:type', 'AccountController.confirm').as('account.confirm')
}).prefix('account')

Route.group('settings', () => {
  Route.get('', 'SettingController.index').as('settings.index')
  Route.put('', 'SettingController.update').as('settings.update')
}).prefix('settings').middleware(['auth', 'api'])

Route.resource('users', 'UserController').apiOnly().middleware(['auth', 'api'])
Route.resource('users.roles', 'UserController').apiOnly().middleware(['auth', 'api'])
Route.resource('roles', 'RoleController').apiOnly().middleware(['auth', 'api'])
Route.resource('roles.permissions', 'RoleController').apiOnly().middleware(['auth', 'api'])
Route.resource('permissions', 'PermissionController').apiOnly().middleware(['auth', 'api'])

Route.resource('people', 'PersonController').apiOnly().middleware(['auth', 'api'])

// Route.get('/', ({ request, view }) => {
//   const greeting = 'Hello World'

//   if (request.accepts(['html', 'json']) === 'json') {
//     return { greeting }
//   }

//   return view.render('welcome', { greeting })
// })

Route.get('*', async ({ request, response }) => {
  /**
   * Do not end the response when this method has been executed.
   * Nuxt will write the response in background and will close
   * the response when required.
   *
   * @type {Boolean}
   */
  response.implicitEnd = false

  /**
   * Finally handover request to nuxt
   */
  await use('App/Services/Nuxt').render(request, response)
})
