'use strict'

const Event = use('Event')

Event.on('account:activation', 'Account.requestActivation')
Event.on('account:invitation', 'Account.proceedInvitation')
Event.on('account:updateEmail', 'Account.requestEmail')
Event.on('account:updatePassword', 'Account.requestPassword')
Event.on('account:confirmed', 'Account.confirmed')
