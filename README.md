# Simple As F*ck Application

[![pipeline status](https://gitlab.com/feryardiant/saf/badges/master/pipeline.svg)](https://gitlab.com/feryardiant/saf/commits/master)
[![coverage report](https://gitlab.com/feryardiant/saf/badges/master/coverage.svg)](https://gitlab.com/feryardiant/saf/commits/master)

Yet another simple application built on top [Adonis.js 4.1.0](http://adonisjs.com)

## Requirements

- **Node.js** 8.9 or greater
- **NPM** 5.6 or greater
- **Adonis.js** 4.1 (installed globally)

## Installation

- Clone this repo and enter project root directory,
- Run `npm install` command to install all dependencies,
- Copy and rename `.env.example` to `.env`,
- Setup your configurations,
- Run `adonis key:generate` to generate app key,
- Run `node ace migration:run` to install migrations,
- Run `node ace seed --files InitSeeder.js` to Initialize data,
- Enjoy

## Commands

- Start Development Server

    ```bash
    $ npm run serve // or `npm run serve:dbg` to enable debuging mode
    ```

- Testing & Linting

    ```bash
    $ npm test
    $ npm run lint
    ```

## License

This project is open-sourced software licensed under the [MIT license](license.md).
