'use strict'

const watch = [
  'app',
  'config',
  'providers',
  'resources',
  'start'
]

module.exports = {
  apps: [
    {
      name: 'back-end',
      script: 'server.js',
      instances: process.env.WEB_CONCURRENCY || 2,
      exec_mode: 'cluster',
      max_memory_restart: '150M',
      watch,
      watch_options: {
        usePolling: true,
        atomic: false,
        followSymlinks: false
      },
      env: {
        'NODE_ENV': 'production'
      },
      env_development: {
        'NODE_ENV': 'development'
      }
    },
    {
      name: 'queue',
      script: 'ace',
      args: 'kue:listen'
    }
  ]
}
