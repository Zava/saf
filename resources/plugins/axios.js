'use strict'

import { defaults, interceptors } from 'axios'

export default async ({ store, redirect, error }) => {
  defaults.headers.common['Accept'] = 'application/json'
  defaults.headers.common['Content-Type'] = 'application/json'

  interceptors.response.use((response) => {
    // Do something with response data
    const token = response.headers['x-api-token'] || store.getters.authToken

    if (token) {
      defaults.headers.common['Authorization'] = `Bearer ${token}`
    }

    return response
  }, (err) => {
    if (err.statusCode === 401) {
      return redirect('/login')
    }

    if (err.statusCode >= 500) {
      return error(err)
    }

    throw err
  })
}
