'use strict'

const dashToDot = (str, defaults = '') => {
  if (typeof str !== 'string') {
    return defaults
  }

  if (['index', 'login', 'forgot-password'].indexOf(str) > -1) {
    return `app.${str}`
  }

  const arr = str.split(/-/g)

  if (arr.length < 3 && arr[0] !== 'index') {
    arr.push('index')
  }

  if (arr.length === 3) {
    arr.shift()
  }

  return arr.join('.')
    .replace(/new$/, 'create')
    .replace(/id$/, 'edit')
}

module.exports = function (routes) {
  return routes.forEach(route => {
    if (route.name) {
      route.name = dashToDot(route.name)
    }

    if (route.children instanceof Array) {
      route.children.forEach(child => {
        if (child.name) {
          child.name = dashToDot(child.name)
        }
      })
    }
  })
}
