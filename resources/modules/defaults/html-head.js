

const meta = []

const link = [
  {
    rel: 'shortcut icon',
    type: 'image/x-icon',
    href: '/favicon.ico'
  },
  {
    rel: 'apple-touch-icon',
    size: '192x192',
    href: '/mobile-icon.png'
  },
  {
    rel: 'icon',
    type: 'image/png',
    size: '192x192',
    href: '/mobile-icon.png'
  },
  {
    rel: 'icon',
    type: 'image/png',
    size: '32x32',
    href: '/favicon.png'
  }
]

module.exports = (head, { locale, project }) => {
  head.htmlAttrs = {
    lang: locale || 'en'
  }

  // if (project.description) {
  //   meta.push({
  //     hid: 'description',
  //     name: 'description',
  //     content: project.description
  //   })
  // }

  head.title = project.title

  head.meta = head.meta.concat(meta)

  head.link = head.link.concat(link)

  head.noscript = [{
    innerHtml: [
      '<h1>Javascript Disabled</h1>',
      '<p>It appears that you do not have Javascript enabled. This application relies on Javascript for most of our features.<p>',
      '<p>Please enable Javascript and <a href=".">reload</a> in order to use this site.</p>'
    ].join('')
  }]
}
