'use strict'

const { resolve } = require('path')

const assignHead = require('./html-head')
const routing = require('./routing')

module.exports = function () {
  const options = this.nuxt.options

  options.loading.color = options.env.color

  assignHead(options.head, options.env)

  this.extendRoutes(routing)

  this.addPlugin(resolve(__dirname, 'plugins.js'))
}
