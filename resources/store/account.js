'use strict'

import axios from 'axios'

export const state = () => ({
  model: {}
})

export const mutations = {
  stored (state, model) {
    state.model = model
  }
}

export const actions = {
  async fetch ({ commit }) {
    const { data } = await axios.get('/account')

    commit('stored', data)
  }
}
