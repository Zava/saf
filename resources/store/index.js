'use strict'

// import * as types from '../mutation_types'
import axios from 'axios'
import Persist from 'vuex-persist'

const persistancePlugin = (arg) => {
  if (!process.client) return () => {}

  const persistence = new Persist({
    key: 'api-token',

    reducer (state) {
      return state.$auth.token
    },

    filter (mutation) {
      return ['authenticated', 'invalidate'].includes(mutation.type)
    }
  })

  return persistence.plugin
}

export const plugins = [
  persistancePlugin
]

export const strict = false

export const state = () => ({
  $auth: {},
  $alerts: {}
})

export const getters = {
  hasAlerts (state) {
    return state.alerts && Object.keys(state.alerts).length > 0
  },

  authCheck (state, getters) {
    return !!getters.authToken
  },

  authToken (state) {
    const storedToken = process.client && localStorage.getItem('api-token')
    return state.$auth.token || storedToken
  }
}

export const mutations = {
  authenticated (state, { token, account }) {
    state.$auth.token = token
    state.$auth.account = account
  },

  invalidate (state) {
    state.$auth = {}
  },

  notify (state, alerts) {
    state.$alerts = alerts
  }
}

export const actions = {
  async login ({ commit }, credential) {
    const { data } = await axios.post('/account/login', credential)

    commit('authenticated', {
      token: data.api_token,
      account: data.account
    })
    commit(`account/stored`, data.account, { root: true })
  },

  async logout ({ commit }) {
    try {
      await axios.post('/account/logout')

      commit('invalidate')
    } catch ({ message, response }) {
      commit('notify', { message, errors: response.data })
    }
  }
}
