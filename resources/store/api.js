'use strict'

// import * as types from '../mutation_types'
import axios from 'axios'
import download from 'downloadjs'
import Qs from 'qs'

export const state = () => ({
  $meta: {
    checkedRows: [],
    validationErrors: {},
    isLoading: false,
    includes: {}
  }
})

export const mutations = {
  /**
   * @param {Object}  state   Store states
   * @param {Boolean} started parameters
   */
  loading (state, started) {
    state.$meta.checkedRows = []
    state.$meta.isLoading = !!started
    state.$meta.validationErrors = {}
  },

  assignRelations (state, { type, includes }) {
    state.$meta.includes[type] = includes || []
  },

  invalidFields (state, fields) {
    state.$meta.validationErrors = fields
    state.$meta.isLoading = false
  }
}

const api = axios.create({
  baseURL: ''
})

const fetchData = (type, params, headers = {}) => {
  return api.get(`/${type}`, {
    headers,
    params,
    paramsSerializer (params) {
      return Qs.stringify(params, { indices: true })
    },
    validateStatus (status) {
      return status === 404 || (status >= 200 && status <= 300)
    }
  })
}

export const actions = {
  async fetch ({ commit }, { type, sorting, page, limit, search, includes }) {
    commit('loading', true)

    sorting = `${sorting.field}:${sorting.order}`
    const payload = { search, sorting }

    const { data } = await fetchData(type, {
      sorting, page, limit, search, includes
    })

    payload.data = data.data
    payload.pagination = {
      total: data.total ? Number(data.total) : 0,
      current: data.page ? Number(data.page) : page,
      limit: data.perPage ? Number(data.perPage) : limit,
      last: data.lastPage ? Number(data.lastPage) : null
    }

    commit('assignRelations', { type, includes })
    commit(`${type}/fetched`, payload, { root: true })

    return payload
  },

  async excel ({ commit }, { type, sorting, page, limit, search, includes }) {
    commit('loading', true)

    sorting = `${sorting.field}:${sorting.order}`

    const excel = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    const { data } = await fetchData(type, {
      sorting, page, limit, search, includes
    }, { 'Accept': excel })

    commit('loading', false)

    download(data, 'export.xlsx', excel)

    return data
  },

  async relations ({ commit }, { type, relations }) {
    commit('loading', true)

    const related = {}

    for (const relation of relations) {
      let { data } = await api.get(`/${relation}`)

      related[relation] = data
    }

    commit(`${type}/relationFetched`, related, { root: true })

    return related
  },

  async create ({ commit }, { type, input }) {
    commit('loading', true)

    const { data } = await api.post(`/${type}`, input)

    commit(`${type}/stored`, data, { root: true })

    return data
  },

  async show ({ commit }, { type, id, includes = [] }) {
    commit('loading', true)

    const param = id ? `${type}/${id}` : type
    const { data } = await api.get(`/${param}`, {
      params: { includes },
      paramsSerializer (params) {
        return Qs.stringify(params, { indices: true })
      }
    })

    commit(`${type}/stored`, data, { root: true })

    return data
  },

  async update ({ commit }, { type, id, input }) {
    commit('loading', true)

    const param = id ? `${type}/${id}` : type
    const { data } = await api.put(`/${param}`, input)

    commit(`${type}/stored`, data, { root: true })

    return data
  },

  async delete ({ commit }, { type, id }) {
    commit('loading', true)

    const param = id ? `${type}/${id}` : type
    const { data } = await api.delete(`/${param}`)

    commit(`${type}/stored`, data, { root: true })
  }
}
