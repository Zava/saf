'use strict'

export const state = () => ({
  model: {},
  relations: {}
})

export const mutations = {
  relationFetched (state, relations) {
    state.relations = relations
  },

  stored (state, model) {
    state.model = model
  }
}
