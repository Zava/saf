'use strict'

// import download from 'downloadjs'
import Resource from './resource'

const EmptyData = () => import('~/components/empty-data')

export default {
  mixins: [Resource],

  components: { EmptyData },

  computed: {
    $searchable () {
      return !!this.pagination.total || (this.search && !!this.search.length)
    }
  },

  methods: {
    fetched () {
      this.$store.commit('api/loading', false)
    },

    fetch (options, download = false) {
      options = Object.assign({}, {
        type: this.$resource.name,
        search: this.search,
        includes: this.$relations,
        sorting: this.sorting,
        limit: this.pagination.limit,
        page: this.pagination.current
      }, options || {})

      return this.$store.dispatch((download === false ? 'api/fetch' : 'api/excel'), options)
    },

    sort (field, order) {
      this.fetch({
        sorting: { field, order }
      }).then(this.fetched).catch(this.failed)
    },

    page (page) {
      this.fetch({ page }).then(this.fetched).catch(this.failed)
    },

    filter (search) {
      this.fetch({ search }).then(this.fetched).catch(this.failed)
    },

    exportData () {
      // const excel = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'

      this.fetch({}, true).then(this.fetched).catch(this.failed)
    },

    remove (items) {
      this.$dialog.confirm({
        message: 'Apakah anda yakin ingin menghapus data tersebut?',
        confirmText: 'Hapus',
        type: 'is-danger',
        hasIcon: true,
        onConfirm: () => {
          Promise.all(items.map((item) => {
            return this.$store.dispatch('api/delete', {
              type: this.$resource.name,
              id: item.id
            })
          })).then(() => {
            this.$snackbar.open({
              type: 'is-success',
              message: 'Data berhasil dihapus'
            })

            this.fetch()
          }).catch(this.failed)
        }
      })
    }
  }
}
