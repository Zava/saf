'use strict'

export default {
  methods: {
    notify (type, message, actionText = 'Ok', onAction = null) {
      const snackbar = { type, message, actionText, position: 'is-bottom-left' }

      if (typeof onAction === 'function') {
        snackbar.onAction = onAction
      }

      this.$snackbar.open(snackbar)
    }
  }
}
