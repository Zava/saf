'use strict'

export default {
  methods: {
    getType (field) {
      return this.getError(field) ? 'is-danger' : ''
    },

    getMessage (field, defaults = null) {
      return this.getError(field) || defaults
    },

    getError (field) {
      return this.$errors.hasOwnProperty(field) && this.$errors[field]
    },

    _showAlert (type, message, actionText = 'Ok', onAction = null) {
      const snackbar = { type, message, actionText, position: 'is-bottom-left' }

      if (typeof onAction === 'function') {
        snackbar.onAction = onAction
      }

      this.$snackbar.open(snackbar)
    }
  }
}
