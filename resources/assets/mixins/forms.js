'use strict'

import Resource from './resource'

const Heading = () => import('~/components/heading')

export default {
  components: { Heading },

  mixins: [Resource],

  methods: {
    fieldType (field) {
      return this.isInvalid(field) ? 'is-danger' : ''
    },

    fieldMessage (field, message) {
      return this.$meta.validationErrors[field] || message || ''
    },

    isInvalid (field) {
      return Array.isArray(this.$meta.validationErrors[field])
    },

    update () {
      this.$store.dispatch('api/update', {
        type: this.$resource.name,
        id: this.model.id,
        input: this.model
      }).then((data) => {
        this.reload(data.id)

        this.$snackbar.open({
          type: 'is-success',
          message: 'Data berhasil diperbarui'
        })

        this.$store.commit('api/loading', false)
      }).catch(this.failed)
    },

    create () {
      this.$store.dispatch('api/create', {
        type: this.$resource.name,
        input: this.model
      }).then((data) => {
        this.$router.push(this.$resource.append(data.id))

        this.$snackbar.open({
          type: 'is-success',
          message: 'Data berhasil disimpan'
        })

        this.$store.commit('api/loading', false)
      }).catch(this.failed)
    },

    reload (id) {
      this.$store.dispatch('api/show', {
        id: id,
        type: this.$resource.name,
        includes: this.$relations
      }).then(this.fetched).catch(this.failed)
    },

    remove (item) {
      this.$dialog.confirm({
        message: 'Apakah anda yakin ingin menghapus data tersebut?',
        confirmText: 'Hapus',
        type: 'is-danger',
        hasIcon: true,
        onConfirm: () => {
          this.$store.dispatch('api/delete', {
            type: this.$resource.name,
            id: item.id
          }).then(() => {
            this.$snackbar.open({
              type: 'is-success',
              message: 'Data berhasil dihapus'
            })

            this.$router.push(this.$resource.path)
          }).catch(this.failed)
        }
      })
    },

    cancel () {
      this.$router.push(this.$resource.path)
    }
  }
}
