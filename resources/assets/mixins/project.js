'use strict'

export default {
  created () {
    this.updatePageProperty()
  },

  computed: {
    $project () {
      return process.env.project || {}
    }
  },

  watch: {
    '$route': 'updatePageProperty'
  },

  methods: {
    updatePageProperty () {
      if (typeof document !== 'undefined') {
        document.querySelector('body')
          .setAttribute('data-page', this.$route.name)
      }
    },

    pageTitle (page = null) {
      const { title, company } = this.$project

      if (this.$route.meta.title) {
        return `${this.$route.meta.title} | ${title}`
      }

      if (page === null) {
        page = company
      }

      return `${page} | ${title}`
    }
  }
}
