'use strict'

import { mapState } from 'vuex'

const errors = {
  400: 'Pengisian formulir belum sesuai'
}

export default {
  mounted () {
    // console.log(this.$resource) // eslint-disable-line no-console
  },

  computed: {
    $resource () {
      const parent = this.$route.matched.find(r => r.parent === undefined)
      const hasParent = parent.path !== this.$route.path
      const resource = {}

      resource.name = this.$route.name.split('.').shift()
      resource.path = hasParent
        ? [parent.path, resource.name].join('/')
        : this.$route.path

      resource.append = (param) => {
        if (['string', 'number'].indexOf(typeof param) < 0) {
          return resource.path
        }

        return [resource.path, param].join('/')
      }

      if (hasParent) {
        resource.parent = {
          name: parent.name,
          path: parent.path
        }
      }

      return resource
    },

    $relations () {
      return this.$meta.includes[this.$resource.name] || []
    },

    ...mapState('api', ['$meta'])
  },

  methods: {
    failed (err) {
      const msg = 'Terjadi masalah operasional, segera laporkan ke administrator'

      if (!err.response) {
        this.$snackbar.open({ type: 'is-danger', message: msg })
        console.error(err) // eslint-disable-line no-console

        return
      }

      const { status, data } = err.response

      this.$snackbar.open({
        type: 'is-danger',
        message: errors[status] || msg
      })

      if (status === 400) {
        const fields = {}

        for (const { field, message } of data) {
          if (!fields[field]) {
            fields[field] = []
          }

          fields[field].push(message)
        }

        this.$store.commit('api/invalidFields', fields)
      } else {
        console.error(err.response) // eslint-disable-line no-console

        this.$store.commit('api/loading', false)
      }
    },

    notify (type, message, actionText = 'Ok', onAction = null) {
      const snackbar = { type, message, actionText, position: 'is-bottom-left' }

      if (typeof onAction === 'function') {
        snackbar.onAction = onAction
      }

      this.$snackbar.open(snackbar)
    }
  }
}
