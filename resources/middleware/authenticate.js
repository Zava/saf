'use strict'

export default function ({ store, redirect }) {
  if (!store.getters.authCheck) {
    return redirect('/login')
  }
}
